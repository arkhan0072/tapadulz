@extends('frontend.master')
@section('content')


    @include('frontend.sliders')

    <section class="seller"><!-- start .seller -->
        <div class="carousel">
            <div class="container">
                <div class="seller-area mx-auto mb-4">
                    <div class="title">
                        <h4>Top Rated Sellers</h4>
                    </div>

                    <div class="slider-container seller-container px-5">
                        <div class="owl-carousel owl-theme seller-carousel ">
                            <div class="seller-item">
                                <div id="seller1-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33"
                                                         height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller2-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33"
                                                         height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller3-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33"
                                                         height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller4-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33"
                                                         height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller5-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406"
                                                     height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33"
                                                         height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seller-area mx-auto mb-4">
                    <div class="title">
                        <h4>Recently Viewed & More..</h4>
                    </div>


                    <div class="slider-container seller-container px-5">
                        <div class="owl-carousel owl-theme seller-carousel ">

                                @foreach($services as $key=>$serveic)
                                    <div class="seller-item">
                                        <div id="seller{{$serveic->id}}{{$key+1}}-slider" class="flexslider">
                                            <ul class="slides">
                                                @foreach($serveic->pictures as $key1=>$picture)

                                                    @if($picture)
                                                        <li>
                                                            <figure>
                                                                <img
                                                                    src="{{asset('public/images/services/')}}/{{$picture->name}}"
                                                                    alt="" width="406"
                                                                    height="262">
                                                            </figure>
                                                        </li>
                                                    @else
                                                        <li>
                                                            <figure>
                                                                <img src="{{asset('public/images/services/default.png')}}"
                                                                     alt="" width="406"
                                                                     height="262">
                                                            </figure>
                                                        </li>

                                                    @endif
                                                    @section('scripts')
                                                        <script>
                                                            $(document).ready(function () {

                                                                $('#seller{{$serveic->id}}{{$key+1}}-slider').flexslider({
                                                                    animation: "slide",
                                                                    controlNav: true,
                                                                    directionNav: true,
                                                                    slideshowSpeed: 5000,
                                                                    animationSpeed: 2000,
                                                                    start: function(slider){
                                                                        $('body').removeClass('loading');
                                                                    }
                                                                });

                                                            });
                                                        </script>
                                                    @endsection
                                                @endforeach
                                            </ul>
                                        </div>

                                        <div class="seller-info">
                                            <div class="user clearfix">
                                                <figure>
                                                    <img src="{{asset('public/images/user.png')}}" alt="" width="67"
                                                         height="66">
                                                </figure>

                                                <div class="user-name clearfix">
                                                    <div class="info">
                                                        <h5>{{$serveic->user->name}}</h5>
                                                        <p>Top Rated Seller</p>
                                                    </div>

                                                    <a href="#">
                                                        <figure>
                                                            <img src="{{asset('public/images/heart.png')}}" alt="" width="33"
                                                                 height="29">
                                                        </figure>
                                                    </a>
                                                </div>
                                            </div>

                                            <h4><a href="{{route('viewgig',$serveic->slug)}}">{{str_limit($serveic->details,150,'...')}}</a></h4>

                                            <div class="price clearfix">
                                                {{$ratingz[$key]}}
                                                    <ul>
                                                        <li class="nav">
{{--                                                            <span class="pr-3">write something about</span>--}}
                                                            <select class="stars stars-example-fontawesome example-fontawesome" name="q1" autocomplete="off">
                                                                <option value="1" {{$ratingz[$key] == 1 ? 'selected':''}}>1</option>
                                                                <option value="2" {{$ratingz[$key] == 2 ? 'selected':''}}>2</option>
                                                                <option value="3" {{$ratingz[$key] == 3 ? 'selected':''}}>3</option>
                                                                <option value="4" {{$ratingz[$key] == 4 ? 'selected':''}}>4</option>
                                                                <option value="5" {{$ratingz[$key] == 5 ? 'selected':''}}>5</option>
                                                            </select>
                                                        </li>
                                                    </ul>

                                                <span>$1,000</span>
                                            </div>
                                        </div>
                                    </div>


                                @endforeach
                            </div>

                            </div>
                        </div>
                    </div>
                </div>

    </section><!-- //end .seller -->

    <section class="service"><!-- start service -->
        <figure>
            <img src="{{asset('public/images/service.png')}}" alt="" width="1834" height="726">
        </figure>

        <div class="service-area">
            <div class="vertical-column">
                <div class="vertical-cell">
                    <div class="container">
                        <div class="service-info mx-auto">
                            <h3 class="text-center text-white">Find Freelance Services For Your Business Today</h3>
                            <p class="text-center text-white">We've got you covered for all your business needs</p>

                            <div class="service-area-link mx-auto">
                                <a class="text-white text-center" href="#">Get Started!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .service -->

    <section class="explore"><!-- start explore -->
        <div class="container">
            <div class="heading">
                <h3>Explore Tapadul</h3>
            </div>

            <div class="explore-area mx-auto">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/graphic.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Graphics & Design</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/marketing.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Digital Marketing</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/writing.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Writing & Translations</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/video.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Video & Animarions</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/music.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Music & Audio</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/tech.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Programmign & Tech</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/business.png')}}" alt="" width="151" height="144">
                                </figure>

                                <h4 class="text-center">Business</h4>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href="#">
                            <div class="explore-item">
                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/lifestyle.png')}}" alt="" width="113" height="144">
                                </figure>

                                <h4 class="text-center">Lifestyle</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .explore -->

    <section class="special-links"><!-- start special-links -->
        <div class="container">
            <div class="special-links-area mx-auto">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item category">
                            <h4>Catageories</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item about">
                            <h4>About</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item support">
                            <h4>Support</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item community">
                            <h4>Community</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .special-links -->
@endsection
@section('scriptz')
    <script>
        $(function() {
            $('.example-fontawesome').barrating({
                theme: 'fontawesome-stars',
                readonly: true
            });
        });
        $(document).ready(function () {
            var ab='{{$ratingz[0]}}';

        if (ab<1){
            $('.br-readonly').find('a').removeClass('br-selected br-current');
        }
        })
    </script>

    @endsection
