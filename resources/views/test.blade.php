<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tapadul | Home</title>
    <link rel="shortcut icon" href="{{asset('public/images/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,300i,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('public/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/responsive.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />

</head>

<body>
<div id="wrapper"><!-- start wrapper -->
    <header class="bg-white">
        <div class="container">
            <div class="header-area mx-auto"><!-- start .header-area -->
                <div class="header-top clearfix">
                    <div class="logo-area clearfix">
                        <div class="logo">
                            <a href="index.html">
                                <figure>
                                    <img src="images/logo.png" alt="" width="297" height="159">
                                </figure>
                            </a>
                        </div>

                        <form class="clearfix">
                            <div class="form-group">
                                <input type="submit" value="" class="search border-0 px-0">

                                <input type="text" class="form-control py-0" placeholder="Find services you need!">
                            </div>

                            <button class="btn py-0">Search</button>
                        </form>
                    </div>

                    <div class="header-top-right">
                        <div class="clearfix">
                            <ul class="nav">
                                <li><a class="px-0 py-0" href="#">Home</a></li>
                                <li><a class="px-0 py-0" href="#">Community</a></li>
                                <li><a class="px-0 py-0" href="#">Messages</a></li>
                                <li><a class="px-0 py-0" href="#">Orders</a></li>
                            </ul>
                        </div>


                        <div class="user mb-0 ml-auto d-none d-lg-block clearfix">
                            <div class="user-name">
                                <h5>Full Name</h5>
                                <h6>username</h6>
                            </div>

                            <figure>
                                <img src="images/user2.png" alt="" width="87" height="86">
                            </figure>
                        </div>
                    </div>
                </div>
            </div><!-- //end .header-area -->
        </div>

        <div class="menu"><!-- start .header-area -->
            <div class="container">
                <div class="menu-area">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="text-center text-white" href="#">Inbox</a>
                        </li>

                        <li class="nav-item marketing">
                            <a class="text-center text-white" href="#">Saved</a>
                        </li>

                        <li class="nav-item writing">
                            <a class="text-center text-white" href="#">Sent</a>
                        </li>

                        <li class="nav-item video">
                            <a class="text-center text-white" href="#">Peding</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- //end .header-area -->
    </header>

    <section class="massage"><!-- start .massage -->
        <div class="container">
            <div class="messaging mx-auto">
                <div class="inbox_msg">
                    <div class="inbox_people">
                        <div class="icons mx-auto">
                            <ul class="nav">
                                <li class="mx-3">
                                    <a href="#">
                                        <figure>
                                            <img src="images/plus.png" alt="" width="28" height="28">
                                        </figure>
                                    </a>
                                </li>

                                <li class="mx-3 mt-1">
                                    <a href="#">
                                        <figure>
                                            <img src="images/envelope.png" alt="" width="36" height="23">
                                        </figure>
                                    </a>
                                </li>

                                <li class="mx-3">
                                    <a href="#">
                                        <figure>
                                            <img src="images/user-icon.png" alt="" width="29" height="33">
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="inbox_chat">
                            <div class="chat_list active_chat">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>

                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>

                            <div class="chat_list">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>
                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>

                            <div class="chat_list">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>
                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>

                            <div class="chat_list">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>
                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>

                            <div class="chat_list">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>
                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>

                            <div class="chat_list">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>
                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>

                            <div class="chat_list">
                                <div class="chat_people">
                                    <div class="clearfix mb-2">
                                        <figure class="chat_img">
                                            <img src="images/user.png" alt="" width="67" height="66">
                                        </figure>

                                        <div class="chat_ib">
                                            <h5>Freelancer username <span class="chat_date">Active</span></h5>
                                        </div>
                                    </div>

                                    <p>Message preview will go right here..</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mesgs">
                        <div class="massage-body border-0 px-0 py-0">
                            <div class="heading clearfix mb-5">
                                <figure>
                                    <img src="images/user2.png" alt="" width="87" height="86">
                                </figure>

                                <div class="user-name mt-3">
                                    <h5>Freelancer username</h5>
                                    <span>Active</span>
                                </div>
                            </div>
                        </div>

                        <div class="msg_history">
                            <div class="incoming_msg">
                                <figure class="incoming_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>

                            <div class="outgoing_msg">
                                <figure class="outgoing_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="sent_msg">
                                    <div class="send_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>

                            <div class="incoming_msg">
                                <figure class="incoming_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>

                            <div class="outgoing_msg">
                                <figure class="outgoing_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="sent_msg">
                                    <div class="send_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>

                            <div class="incoming_msg">
                                <figure class="incoming_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>

                            <div class="outgoing_msg">
                                <figure class="outgoing_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="sent_msg">
                                    <div class="send_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>

                            <div class="incoming_msg">
                                <figure class="incoming_msg_img">
                                    <img src="images/user.png" alt="" width="67" height="66">
                                </figure>

                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>Hey, how are you doing?</p>
                                        <p>What can I do for you today?</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="massage-body border-0 px-0 py-0">
                            <form>
                                <div class="row mx-0">
                                    <div class="col-11 px-0">
                                        <div class="form-group msg-text">
												<textarea class="form-control" placeholder="Type your text here!">

												</textarea>
                                        </div>
                                    </div>

                                    <div class="col-1 px-0">
                                        <div class="element">
                                            <div class="expression my-2">
                                                <a href="#">
                                                    <figure>
                                                        <img src="images/express.png" alt="" width="46" height="42">
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="upload my-2">
                                                <a href="#">
                                                    <figure>
                                                        <img src="images/upload.png" alt="" width="44" height="54">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button class="btn text-white ml-auto">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .massage -->

</div><!-- //end #wrapper -->

<script src="{{asset('public/js/jquery.min.js')}}"></script>
<script src="{{asset('public/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/jquery.flexslider.js')}}"></script>
<script src="{{asset('public/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('public/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('public/js/custom.js')}}"></script>
</body>
</html>
