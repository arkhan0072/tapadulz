@extends('frontend.master')
@section('content')
    <style>
        select {
            border: 3px solid #17839e!important;
            border-radius: 20px!important;
            padding: 6px!important;
            width: 140px!important;
        }
    </style>
    <section class="seller seller2 pt-2"><!-- start .seller -->
        <div class="container">
            <div class="filter mx-auto">
                <form class="clearfix px-0 mt-3 mb-4">
                    <div class="form-group">
                        <input type="submit" value="" class="search border-0 px-0">

                        <input type="text" name="key" class="form-control py-0 key1" placeholder="Find services you need!">

                        <span class="text-right">Advance search</span>
                    </div>

                    <button class="btn py-0" id="search">Search</button>
                </form>

                <div class="filter-area mx-auto mb-2">
                    <div class="row">
                        <div class="col-md-8 px-0">
                            <ul class="nav">
                                <li class="dropdown">
                                        <select name="category" class="category">
                                            <option value="">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>

                                </li>

                                <li class="dropdown">
                                    <select name="amount" class="amount">
                                        <option value="">Select Budget</option>
                                        @for($i=5;$i<=$max;$i+=5)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </li>

                                <li class="dropdown">
                                    <select name="duration" class="duration">
                                        <option value="">Select duration</option>
                                        @for($i=5;$i<=$maxDuration;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </li>

                                <li class="dropdown">
                                    <select name="language" class="language">
                                        <option value="">Select Language</option>
                                            <option value="English">English</option>
                                            <option value="Arabic">Arabic</option>
                                    </select>
                                </li>

{{--                                <li class="dropdown">--}}
{{--                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Fliter 5</span></a>--}}

{{--                                    <div class="dropdown-menu">--}}
{{--                                        <a class="dropdown-item" href="#">Item 1</a>--}}
{{--                                        <a class="dropdown-item" href="#">Item 2</a>--}}
{{--                                        <a class="dropdown-item" href="#">Item 3</a>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
                            </ul>
                        </div>

                        <div class="col-md-4 px-0">
                            <div class="switch-area clearfix mt-3">
                                <div class="switch-item">
                                    <span>Pro Sellers</span>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>

                                <div class="switch-item">
                                    <span>Online Sellers</span>
                                    <label class="switch">
                                        <input type="checkbox" class="status" value="">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="carousel">
            <div class="container">
                <div class="sort text-right">
                    <a href="#">Sort by <strong>Relevence</strong></a>
                </div>

                <div class="seller-area mx-auto mb-4">
                    <div class="seller-container px-5">

                    <div class="html"></div>
                        </div>
                    </div>
                </div>
        </div>
    </section><!-- //end .seller -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            var key,category,duration,amount,language,status;

            $.ajax({
                url:'{{route('searchAjax')}}',
                type:'post',
                data:{
                    '_token':'{{csrf_token()}}',
                    key:'{{$_GET['key'] ? $_GET['key']:''}}',
                    category:category,
                    duration:duration,
                    amount:amount,
                    language:language,
                    status:status,
                    limit:10
                },
                success: function (data) {
                    console.log(data);
                    $('.html').html(data);
                }
            })
            $('.status').click(function () {
                if($(this).val()==null || $(this).val()==''){
                    $(this).val(1);
                }else{
                    $(this).val('');
                }
            })
        $('#search').click(function (e) {

            e.preventDefault();
             key=$('.key1').val();
             category=$('.category').val();
             duration=$('.duration').val();
             amount=$('.amount').val();
             language=$('.language').val();
             status=$('.status').val();

            $.ajax({
                url:'{{route('searchAjax')}}',
                type:'post',
                data:{
                    '_token':'{{csrf_token()}}',
                    key:key,
                    category:category,
                    duration:duration,
                    amount:amount,
                    language:language,
                    status:status,
                    limit:10
                },
                success: function (data) {
                    console.log(data);
                    $('.html').html(data);
                }
        })
        })
        })
    </script>

    @endsection
