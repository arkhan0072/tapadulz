<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-airplay"></i>Services <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('services.create')}}">Add New Service</a>
                        </li>
                        <li>
                            <a href="{{route('services.index')}}">View All Services</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Users <div class="arrow-down"></div></a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{route('users.create')}}">Add New User</a>
                                </li>
                                <li>
                                    <a href="{{route('users.index')}}">View All Users</a>
                                </li>
                            </ul>
                </li>

                <li class="has-submenu">
                    <a href="{{route('offersReceived')}}"> <i class="fe-briefcase"></i>Offers Received <div class="arrow-down"></div></a>
{{--                    <ul class="submenu megamenu">--}}
{{--                        <li>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-buttons">Buttons</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-cards">Cards</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-portlets">Portlets</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-tabs-accordions">Tabs & Accordions</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-modals">Modals</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-progress">Progress</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-notifications">Notifications</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-images">Images</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-carousel">Carousel</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-video">Embed Video</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-dropdowns">Dropdowns</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-spinners">Spinners</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-ribbons">Ribbons</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-tooltips-popovers">Tooltips & Popovers</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-general">General UI</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-typography">Typography</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="ui-grid">Grid</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
                </li>

                <li class="has-submenu">
                    <a href="#"> <i class="fe-cpu"></i>Categories <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('categories.create')}}">Add New Category</a>
                        </li>
                        <li>
                            <a href="{{route('categories.index')}}">View All Categories</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-layers"></i>Skills <div class="arrow-down"></div></a>
                    <ul class="submenu">
                                <li>
                                    <a href="{{route('skills.create')}}">Add New Skill</a>
                                </li>
                                <li>
                                    <a href="{{route('skills.index')}}">View All Skills</a>
                                </li>
                            </ul>
                        </li>
                <li class="has-submenu">
                            <a href="#">Sub Categories <div class="arrow-down"></div></a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{route('subcategories.create')}}">Add New Sub Category</a>
                                </li>
                                <li>
                                    <a href="{{route('subcategories.index')}}">View All SubCategories</a>
                                </li>
                            </ul>
                        </li>

            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->
