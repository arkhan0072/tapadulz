
        @yield('css')

        <!-- App css -->
        <link href="{{ URL::asset('public/ubold/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('public/ubold/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('public/ubold/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
