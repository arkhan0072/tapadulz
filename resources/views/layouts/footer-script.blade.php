        <!-- Vendor js -->
        <script src="{{ URL::asset('public/ubold/assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('public/ubold/assets/js/app.min.js')}}"></script>

        @yield('script-bottom')
