<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tapadul | Home</title>
    <link rel="shortcut icon" href="{{asset('public/images/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,300i,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('public/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/responsive.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />

    @yield('css-styles')
    <style>
        .user img {
            border-radius: 50%;
        }
    </style>
</head>

<body>
<div id="wrapper"><!-- start wrapper -->
    <div class="slider-container seller-container px-5">
        <div class="owl-carousel owl-theme seller-carousel ">
                @foreach($result as $res)

                 <div class="seller-item">
                        <div id="seller{{$res->id}}-slider" class="flexslider">
                            <ul class="slides">
                                @foreach($res->pictures as $picture)
                                <li>
                                    <figure>
                                        <img src="public/images/services/{{$picture->name}}" alt="" width="406" height="262">
                                    </figure>
                                </li>
                                @endforeach


                            </ul>
                        </div>

                        <div class="seller-info">
                            <div class="user clearfix">
                                <figure>
                                    <img src="public/images/users/{{$res->user->picture ? $res->user->picture->name:'user.png'}}" alt="" width="67" height="66">
                                </figure>

                                <div class="user-name clearfix">
                                    <div class="info">
                                        <h5>{{$res->user->username}}</h5>
                                        <p>Top Rated Seller</p>
                                    </div>

                                    <a href="#">
                                        <figure>
                                            <img src="public/images/heart.png" alt="" width="33" height="29">
                                        </figure>
                                    </a>
                                </div>
                            </div>

                            <h4>{!! str_limit($res->details,150,'...') !!}</h4>

                            <div class="price clearfix">
                                <ul class="nav">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>

                                <span>$1,000</span>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>

                </div>
                <script src="{{asset('public/js/jquery.min.js')}}"></script>
                <script src="{{asset('public/bootstrap/js/popper.min.js')}}"></script>
                <script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>
                <script src="{{asset('public/js/jquery.flexslider.js')}}"></script>
                <script src="{{asset('public/js/owl.carousel.min.js')}}"></script>
                <script src="{{asset('public/js/jquery.nicescroll.min.js')}}"></script>
                <script src="{{asset('public/js/custom.js')}}"></script>
</body>
</html>
