@extends('frontend.master')

@section('css-styles')
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}
    <link rel="stylesheet" href="{{asset('public/vendor/messenger/css/messenger.css')}}">
<style>
    .received_msg{
        padding-top: 5px!important;
        padding-bottom: 5px!important;
    }
    img{
        border-radius: 50%!important;
    }
    /*-----------------------------
   massaging
 -----------------------------*/
    .inbox_people {
        float: left;
        overflow: hidden;
        width: 32%;
        border: 2px solid #f5f5f5;
        padding: 40px 10px;
        border-radius: 20px;
        -webkit-border-radius: 20px;
    }
    .inbox_msg {
        clear: both;
        overflow: hidden;
    }
    .top_spac{ margin: 20px 0 0;}
    .chat_ib h5{
        font-size: 24.79px;
        font-weight: 500;
        color: #1b687b;
        line-height: 26px;
    }
    .chat_ib h5 span {
        display: block;
        font-size: 24.79px;
        font-weight: 300;
        color: #1b687b !important;
        font-style: italic;
        line-height: 19px;
    }
    .chat_ib p{ font-size:14px; color:#989898; margin:auto}
    .chat_img {
        float: left;
        width: 21%;
    }
    .chat_ib {
        float: left;
        padding: 13px 0 0 15px;
        width: 79%;
    }
    .chat_people {
        overflow: hidden;
        border: 3px solid #cfcfcf;
        padding: 20px 15px;
    }
    .icons {
        display: table;
        margin-bottom: 20px;
    }
    .chat_people {
        color: #1b687b;
    }
    .chat_list {
        margin: 0;
        padding: 10px 10px 10px 0;
    }
    .inbox_chat {
        height: 700px;
        overflow-y: scroll;
    }
    .incoming_msg_img {
        display: inline-block;
        width: 8.75%;
        float: left;
    }
    .outgoing_msg_img {
        width: 9%;
        float: right;
    }
    .received_msg {
        display: inline-block;
        padding: 0 0 0 20px;
        vertical-align: top;
        width: 90%;
    }
    .received_withd_msg p {
        width: 100%;
        text-align: left;
    }
    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }
    .received_withd_msg {
        background: #f8f8f8;
        width: 44%;
        padding: 15px 20px;
    }
    .mesgs {
        float: left;
        padding: 30px;
        width: 65%;
        border: 2px solid #f5f5f5;
        border-radius: 20px;
        -webkit-border-radius: 20px;
        margin-left: 30px;
    }
    .sent_msg p {
        margin: 0;
        padding: 5px 10px 5px 12px;
        width:100%;
    }
    .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
    .sent_msg {
        float: right;
        width: 42%;
        padding: 0 20px 0 0;
    }
    .send_withd_msg {
        background: #f8f8f8;
    }
    .send_withd_msg p {
        width: 100%;
        text-align: right;
    }
    .input_msg_write textarea {
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 240px;
        width: 100%;
        background: #fff;
    }
    .type_msg {
        position: relative;
        background: #f8f8f8;
        padding: 25px 20px;
    }
    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 11px;
        width: 33px;
    }
    .messaging {
        padding: 0 0 50px 0;
        max-width: 1260px;
    }
    .msg_history {
        height: 516px;
        overflow-y: auto;
        margin-bottom: 50px;
    }
    .incoming_msg_img { position: relative; }
    .incoming_msg_img::before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        width: 18px;
        height: 18px;
        border-radius: 18px;
        background: #008009;
    }
    .chat_img { position: relative; }
    .chat_img::before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        width: 18px;
        height: 18px;
        border-radius: 18px;
        background: #008009;
    }
    .outgoing_msg_img { position: relative; }
    .outgoing_msg_img::before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        width: 18px;
        height: 18px;
        border-radius: 18px;
        background: #008009;
    }

</style>
@endsection
@section('header')
    <div class="menu"><!-- start .header-area -->
        <div class="container">
            <div class="menu-area">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="text-center text-white" href="#">Inbox</a>
                    </li>

                    <li class="nav-item marketing">
                        <a class="text-center text-white" href="#">Saved</a>
                    </li>

                    <li class="nav-item writing">
                        <a class="text-center text-white" href="#">Sent</a>
                    </li>

                    <li class="nav-item video">
                        <a class="text-center text-white" href="#">Peding</a>
                    </li>
                </ul>
            </div>
        </div>
    </div><!-- //end .header-area -->
@endsection
@section('content')

            <div class="messaging mx-auto">
                <div class="inbox_msg">
                    <div class="inbox_people">
                        <div class="icons mx-auto">
                            <ul class="nav">
                                <li class="mx-3">
                                    <a href="#">
                                        <figure>
                                            <img src="{{asset('public/images/plus.png')}}" alt="" width="28" height="28">
                                        </figure>
                                    </a>
                                </li>

                                <li class="mx-3 mt-1">
                                    <a href="#">
                                        <figure>
                                            <img src="{{asset('public/images/envelope.png')}}" alt="" width="36" height="23">
                                        </figure>
                                    </a>
                                </li>

                                <li class="mx-3">
                                    <a href="#">
                                        <figure>
                                            <img src="{{asset('public/images/user-icon.png')}}" alt="" width="29" height="33">
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="inbox_chat">
                        @include('messenger::partials.threads')
                    </div>
                </div>
            <div class="mesgs">

                <div class="massage-body">

                    <div class="panel-heading">
                        <h4 class="d-flex">{{$withUser->name}}
                            @if($withUser->status==1)
                                <img src="{{asset('public/images/status/online.png')}}" width="20px" style="margin-left: 2px;">
                            @else
                                <img src="{{asset('public/images/status/offline.png')}}" width="20px" style="margin-left: 2px;">
                            @endif</h4>
                        @if( is_array($messages) )
                            @if (count($messages) === 20)
                                <div id="messages-preloader"></div>
                            @endif

                            <div id="messages-preloader"></div>
                        @else
                            <p class="start-conv">Conversation started</p>
                        @endif</div>

                    <div class="msg_history">
                        @include('messenger::partials.messages')
                    </div>
                </div>

                <div class="row mx-0">
                    <div class="col-10 px-0">
                        <div class="form-group msg-text">
                            <textarea class="form-control" id="message-body" class="message" name="message" rows="3" placeholder="Type your message..."></textarea>
                        </div>
                    </div>

                    <div class="col-md-2 px-0">
                        <div class="d-flex">
                            {{--                                        <div class="expression my-2">--}}
                            {{--                                            <a href="#">--}}
                            {{--                                                <figure>--}}
                            {{--                                                    <img src="http://tapadulz.ar/public/images/express.png" alt=""--}}
                            {{--                                                         width="46" height="42">--}}
                            {{--                                                </figure>--}}
                            {{--                                            </a>--}}
                            {{--                                        </div>--}}

                            <div class="upload my-2">
                                <a href="#" id="upload">

                                    <figure>
                                        <img src="http://tapadulz.ar/public/images/upload.png" alt=""
                                             width="20">
                                        <input style="display: none;" type="file" accept="image/*" class="attachment" name="attachment[]" multiple/>

                                        <figcaption class="show"></figcaption>
                                        {{--                                                                                            <button class="btn btn-success" id="upload-button">upload files</button>--}}
                                    </figure>

                                </a>

                            </div>
                        </div>
                        <button type="submit" id="send-btn" class="btn btn-primary">SEND</button>
                    </div>
                </div>
            </div>
                </div>
            </div>

    <input type="hidden" name="withId" id="withId" value="{{$withUser->id}}">
@endsection

@section('js-scripts')
    <script src="https://js.pusher.com/5.1/pusher.min.js"></script>
    <script type="text/javascript">
        var withId = '{{$withUser->id}}',
            authId = '{{auth()->id()}}',
            token = '{{csrf_token()}}',
            messagesCount = '{{is_array($messages) ? count($messages) : '0'}}';
        pusher = new Pusher('{{config('messenger.pusher.app_key')}}', {
            cluster: '{{config('messenger.pusher.options.cluster')}}'
        });
    </script>
    <script src="{{asset('public/vendor/messenger/js/messenger-chat.js')}}" charset="utf-8"></script>
    <script>
        {{--$(document).ready(function () {--}}

        {{--    $('#upload-button').click(function (e) {--}}
        {{--        e.preventDefault();--}}
        {{--        var withId = '{{$withUser->id}}',--}}
        {{--            authId = '{{auth()->id()}}';--}}
        {{--        var fileInput = document.getElementsByClassName('attachment');--}}

        {{--        $.ajax({--}}
        {{--            url:"{{route('ChatFile.store')}}",--}}
        {{--            type:'post',--}}
        {{--            data:{--}}
        {{--              '_token':'{{csrf_token()}}' ,--}}
        {{--                withId:withId,--}}
        {{--                authId:authId,--}}
        {{--                conversation:'{{$conversation->id}}',--}}
        {{--                images:fileInput--}}
        {{--            },--}}
        {{--            processData: false,--}}
        {{--            contentType: false,--}}
        {{--            success:function (data) {--}}
        {{--            console.log(data);--}}
        {{--            }--}}
        {{--        })--}}
        {{--    })--}}
        var files;
            $('#upload img').click(function (e) {
                e.preventDefault();
                $('.attachment').trigger('click');

            });
            $('.attachment').change(function () {
               files = $(this)[0].files;
               console.log(files.length);
                $('.show').html(files.length);
                var form_data = new FormData();
                for (var i = 0; i < this.files.length; i++) {
                    let file = this.files[i];
                    form_data.append('files[' + i + ']', file);
                }

                form_data.append("conversation_id",{{$conversation->id}});
                console.log(form_data);
                $.ajax({
                    url:"{{route('ChatFile.store')}}",
                    dataType: 'json', // what to expect back from the PHP script
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'post',
                    data: form_data,
                    processData: false,
                    contentType: false,
                    success:function (data) {
                        console.log(typeof(data));
                        var result='';
                        for (let i = 0; i < data.length; ++i) {
                            result +=data[i]+" "+"\n";
                        }
                        $('#message-body').html(result);
                        $('#send-btn').trigger('click');
                        $('.attachment').val()

                    }
                })
            });

    </script>
@endsection
