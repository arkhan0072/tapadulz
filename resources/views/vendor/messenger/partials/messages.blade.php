@if ($messages)
    @foreach ($messages as $key => $message)
            @if($authId->id==$message->sender_id)
                <div class="outgoing_msg">
                    <figure class="outgoing_msg_img">
                    <img src="{{asset('public/images/users/')}}/{{$authId->picture ? $authId->picture->name:'default.png'}}" alt="" width="67" height="66">
            </figure>

                    <div class="sent_msg">
                        <div class="send_withd_msg">
                            <p title="{{date('d-m-Y h:i A' ,strtotime($message->created_at))}}">
                                {!! html_entity_decode($message->message) !!}
                            </p>
                        </div>
                    </div>
                </div>
            @else
                    <div class="incoming_msg">
                        <figure class="incoming_msg_img">
                            <img src="{{asset('public/images/users/')}}/{{$withUser->picture ? $withUser->picture->name:'default.png'}}" alt="" width="67" height="66">
                        </figure>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                <p title="{{date('d-m-Y h:i A' ,strtotime($message->created_at))}}">
                    {!! html_entity_decode($message->message) !!}
                </p>
                                </div>
                            </div>
                        </div>
            @endif
    @endforeach

@endif

