

    @foreach ($threads as $key => $thread)

        @if ($thread->lastMessage)

            <a href="/messenger/t/{{$thread->withUser->username}}" class="thread-link">
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="clearfix mb-2">
                            <figure class="chat_img">
                            <img src="{{asset('public/images/users/')}}/{{$thread->withUser->picture ? $thread->withUser->picture->name:'default.png'}}" alt="" width="67" height="66">
                            </figure>
                            <div class="chat_ib">
                                <h5>{{$thread->withUser->username}} </h5>
                                @if($thread->withUser->status==1)
                                    <span class="chat_date">Online</span>
                                @else
                                    <span class="chat_date">Offline</span>
                                @endif
                            </div>

                            <p>  @if ($thread->lastMessage->sender_id === auth()->id())
                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                @endif
                                {!! html_entity_decode(substr($thread->lastMessage->message, 0, 20)) !!}
                                @if (!$thread->lastMessage->is_seen &&
                                  $thread->lastMessage->sender_id != auth()->id())
                                    (<i class="fa fa-check"></i>)
                                @else
                                    (<i class="fa fa-check"></i><i class="fa fa-check"></i>)
                                @endif</p>
                        </div>
                    </div>
                </div>

            </a>

@endif

@endforeach

