@extends('frontend.master')
@section('content')
<section class="description"><!-- start .description -->
    <div class="container">
        <div class="description-area mx-auto">
            <h3>{{$service->name}}</h3>
            <div class="gig-info clearfix">
                <div class="user-name clearfix">
                    <div class="gig-user clearfix">
                        <figure>
                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                        </figure>

                        <div class="name clearfix">
                            <h5>{{$service->user->name}}</h5>
                            <p>Top Rated Seller</p>
                        </div>
                    </div>

                    <div class="rating clearfix">
                        <ul class="nav">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                    </div>
                </div>

                <div class="info">
                    <h4>Other details of the freelancer goes here</h4>
                </div>
            </div>
        </div>

        <div class="main mx-auto">
            <div class="row">
                <div class="col-md-8">
                    <div id="video-slider" class="flexslider mx-3">
                        <ul class="slides">
                            <li>
                                <div class="slider-area">
                                    <figure>
                                        <img src="{{asset('public/images/video-slider.png')}}" alt="" width="1148" height="692">
                                    </figure>

                                    <div class="play">
                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/play.png')}}" alt="" width="185" height="185">
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="slider-area">
                                    <figure>
                                        <img src="{{asset('public/images/video-slider.png')}}" alt="" width="1148" height="692">
                                    </figure>

                                    <div class="play">
                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/play.png')}}" alt="" width="185" height="185">
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="slider-area">
                                    <figure>
                                        <img src="{{asset('public/images/video-slider.png')}}" alt="" width="1148" height="692">
                                    </figure>

                                    <div class="play">
                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/play.png')}}" alt="" width="185" height="185">
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="px-4 py-3">
                        <div class="owl-carousel owl-theme seller2-carousel">
                            <div class="seller-item">
                                <figure>
                                    <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                </figure>
                            </div>

                            <div class="seller-item">
                                <figure>
                                    <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                </figure>
                            </div>

                            <div class="seller-item">
                                <figure>
                                    <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                </figure>
                            </div>

                            <div class="seller-item">
                                <figure>
                                    <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                </figure>
                            </div>

                            <div class="seller-item">
                                <figure>
                                    <img src="{{asset('public/images/seller-img5.png')}}" alt="" width="406" height="262">
                                </figure>
                            </div>

                            <div class="seller-item">
                                <figure>
                                    <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                </figure>
                            </div>
                        </div>
                    </div>

                    <div class="gig-desc">
                        <h4>Gig Description</h4>

                        <p>{{str_replace('&nbsp;',' ',$service->details)}}.</p>
                    </div>

                    <div class="border"></div>

                    <div class="main-wrap">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="info">
                                    <h5>Category</h5>
                                    <p>{{$service->subcategory->category->name}}</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="info">
                                    <h5>SubCategory</h5>
                                    <p>{{$service->subcategory->name}}</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="info">
                                    <h5>Language</h5>
                                    <p>{{$service->language}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="info">
                                    <h5>Akcent</h5>
                                    <p>American</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="info">
                                    <h5>Age Range</h5>
                                    <p>Adult</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="sidebar">
                        <div class="item">
                            <div class="exchange-link mx-auto">
                                <a class="text-center text-white" href="{{route('viewgig',$service->slug)}}">Exchange Service</a>
                            </div>
                        </div>

                        <div class="item bg-gray">
                            <h5 class="text-center">Do you have any other requirement?</h5>
                            <div class="quote-link mx-auto">
                                <a class="text-white" href="#">Get a quote</a>
                            </div>
                        </div>

                        <div class="item">
                            <h4>About the seller</h4>

                            <figure class="mx-auto">
                                <img src="{{asset('public/images/user3.png')}}" alt="" width="193" height="190">
                            </figure>

                            <div class="user-name">
                                <h5 class="text-center mb-0">{{$service->user->name}}</h5>
                                <p class="text-center">{{$service->user->username}}</p>
                            </div>

                            <div class="rating clearfix">
                                <div class="clearfix">
                                    <ul class="mx-auto">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>

                                <p class="text-center">5.0 of 160 reviews</p>
                            </div>

                            <ul>
                                <li class="clearfix">
                                    <div class="left">
                                        <span>From</span>
                                    </div>

                                    <div class="right">
                                        <span>{{$service->user->city ? $service->user->city->country->name:''}}</span>
                                    </div>
                                </li>

                                <li class="clearfix">
                                    <div class="left">
                                        <span>Member Since</span>
                                    </div>

                                    <div class="right">
                                        <span>{{\Carbon\Carbon::parse($service->user->created_at)->format('Y')}}</span>
                                    </div>
                                </li>

                                <li class="clearfix">
                                    <div class="left">
                                        <span>Avg. Response Time</span>
                                    </div>

                                    <div class="right">
                                        <span>1 Hrs</span>
                                    </div>
                                </li>
                            </ul>

                            <p>{{$service->user->description}}</p>

                            <div class="contact-link mx-auto">
                                <a class="text-center" href="#">Contact me</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- //end .description -->

<section class="seller"><!-- start .seller -->
    <div class="carousel">
        <div class="container">
            <div class="seller-area mx-auto mb-4">
                <div class="title">
                    <h4>Recommended for you</h4>
                </div>

                <div class="seller-container px-5">
                    <div class="owl-carousel owl-theme seller-carousel ">
                        <div class="seller-item">
                            <div id="seller1-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller2-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller3-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller4-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller5-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="seller-area mx-auto mb-4">
                <div class="seller-container px-5">
                    <div class="owl-carousel owl-theme seller-carousel ">
                        <div class="seller-item">
                            <div id="seller6-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262" >
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller7-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller8-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller9-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>

                        <div class="seller-item">
                            <div id="seller10-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>

                                    <li>
                                        <figure>
                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </li>
                                </ul>
                            </div>

                            <div class="seller-info">
                                <div class="user clearfix">
                                    <figure>
                                        <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                    </figure>

                                    <div class="user-name clearfix">
                                        <div class="info">
                                            <h5>Freelancer username</h5>
                                            <p>Top Rated Seller</p>
                                        </div>

                                        <a href="#">
                                            <figure>
                                                <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <h4>Message from freelancer or description of the gig!</h4>

                                <div class="price clearfix">
                                    <ul class="nav">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>

                                    <span>$1,000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- //end .seller -->
    @endsection
