@extends('frontend.master')
@section('content')
    <section class="description"><!-- start .description -->
        <div class="container">
            <div class="description-area mx-auto">
                @if(Session::has('message'))
                    <div class="alert alert-{{ Session::get('message-type') }} alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <i class="glyphicon glyphicon-{{ Session::get('message-type') == 'success' ? 'ok' : 'remove'}}"></i> {{ Session::get('message') }}
                    </div>
                @endif
                <h3>{{$service->name}}</h3>
                <div class="gig-info clearfix">
                    <div class="user-name clearfix">
                        <div class="gig-user clearfix">
                            <figure>
                                <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                            </figure>

                            <div class="name clearfix">
                                <h5>{{$service->user->name}}</h5>
                                <p>Top Rated Seller</p>
                            </div>
                        </div>

                        <div class="rating clearfix">
                            <ul class="nav">
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                    </div>

                    <div class="info">
                        <h4>Other details of the freelancer goes here</h4>
                    </div>
                </div>
            </div>

            <div class="main mx-auto">
                <div class="row">
                    <div class="col-md-12 col-lg-8">
                        <div id="video-slider" class="flexslider mx-3">
                            <ul class="slides">
                                @if(sizeof($service->pictures))
                                @foreach($service->pictures as $picture)
                                <li>
                                    <div class="slider-area">

                                                <figure>
                                                    <img src="{{asset('public/images/services/')}}/{{$picture->name}}" alt="" width="1148" height="692">
                                                </figure>

                                        <div class="play">
                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/play.png')}}" alt="" width="185" height="185">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                    @else
                                    <li>
                                        <div class="slider-area">

                                            <figure>
                                                <img src="{{asset('public/images/services/default.png')}}" alt="" width="1148" height="692">
                                            </figure>

                                            <div class="play">
                                                <a href="#">
                                                    <figure>
                                                        <img src="{{asset('public/images/play.png')}}" alt="" width="185" height="185">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                @endif


                            </ul>
                        </div>

                        <div class="px-4 py-3">
                            <div class="owl-carousel owl-theme seller2-carousel">
                                @if(sizeof($service->pictures)!=0)
                                @foreach($service->pictures as $picture)
                                <div class="seller-item">
                                    <figure>
                                        <img src="{{asset('public/images/services/')}}/{{$picture->name}}" alt="" width="406" height="262">
                                    </figure>
                                </div>
                                @endforeach
                                    @else
                                    <div class="seller-item">
                                        <figure>
                                            <img src="{{asset('public/images/services/default.png')}}" alt="" width="406" height="262">
                                        </figure>
                                    </div>
                                    @endif


                            </div>
                        </div>

                        <div class="gig-desc">
                            <h4>Gig Description</h4>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                        </div>

                        <div class="border"></div>

                        <div class="main-wrap">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="info">
                                        <h5>Category</h5>
                                        <p>{{$service->subcategory->category->name}}</p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="info">
                                        <h5>SubCategory</h5>
                                        <p>{{$service->subcategory->name}}</p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="info">
                                        <h5>Language</h5>
                                        <p>{{$service->language}}</p>
                                    </div>
                                </div>
                            </div>

{{--                            <div class="row">--}}
{{--                                <div class="col-sm-4">--}}
{{--                                    <div class="info">--}}
{{--                                        <h5>Akcent</h5>--}}
{{--                                        <p>American</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="col-sm-4">--}}
{{--                                    <div class="info">--}}
{{--                                        <h5>Age Range</h5>--}}
{{--                                        <p>Adult</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-4">
                        <div class="sidebar">
                            <div class="item">
                                <div class="row mx-0">
                                    <div class="col-7 px-0">
                                        <h4 class="mb-2">Order Details</h4>
                                    </div>

                                    <div class="col-5 px-0">
                                        <p class="text-right mb-2">$1000</p>
                                    </div>
                                </div>

                                <div class="row mx-0">
                                    <div class="col-6 px-0">
                                        <div class="delivery">
                                            <h4>2 Days Deliver</h4>
                                        </div>
                                    </div>

                                    <div class="col-6 px-0">
                                        <div class="revision">
                                            <h4>Unlimited revisions</h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="contact-form">
                                    <form class="bg-white">
                                        <div class="form-group clearfix">
                                            <label class="text-left">Order Amount</label>
                                            <input class="bg-gray" type="text">
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="text-left">Delivery</label>
                                            <input class="bg-gray" type="date">
                                        </div>
                                    </form>
                                </div>

                                <div class="exchange-link mx-auto">
                                    @if(\Illuminate\Support\Facades\Auth::user())
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$service->id}}" data-service_needed="{{$service->id}}" style="background-color: #17839e!important;border-color: #17839e!important;border-radius: 10px;padding: 10px;">Send Offer</button>
                                    @else
                                        <a href="{{route('login')}}"><button type="button"  class="btn btn-primary" style="background-color: #17839e!important;border-color: #17839e!important;">Login</button> </a>
                                    @endif
                                    <div class="modal fade" id="exampleModal{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form  method="Post">
                                                        <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
                                                        <table class="table table-bordered table-responsive">
                                                            <thead>
                                                            <th>Service</th>
                                                            <th>Duration</th>
                                                            <th>Action</th>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($services as $servicz)
                                                                <tr>
                                                                    <td>{{$servicz->name}}</td>
                                                                    <td>{{$servicz->duration}}</td>
                                                                    <td><input type="submit" class="btn btn-primary idpass" name="Exchange Offer" data-id="{{$service->id}}" data-reciever_id="{{$service->created_by}}" data-service_provided="{{$servicz->id}}" data-service_needed="{{$service->id}}" value="Send offer">
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>

                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    {{--                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                                                    {{--                <button type="button" class="btn btn-primary">Send message</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div class="item bg-gray">
                                <h5 class="text-center">Do you have any other requirement?</h5>
                                <div class="quote-link mx-auto">
                                    <a class="text-white" href="#">Get a quote</a>
                                </div>
                            </div>

                            <div class="item">
                                <h4>About the seller</h4>

                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/user3.png')}}" alt="" width="193" height="190">
                                </figure>

                                <div class="user-name">
                                    <h5 class="text-center mb-0">{{$service->user->name}}</h5>
                                    <p class="text-center">{{$service->user->username}}</p>
                                </div>

                                <div class="rating clearfix">
                                    <div class="clearfix">
                                        <ul class="mx-auto">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>

                                    <p class="text-center">5.0 of 160 reviews</p>
                                </div>

                                <ul>
                                    <li class="clearfix">
                                        <div class="left">
                                            <span>From</span>
                                        </div>

                                        <div class="right">
                                            <span>{{$service->user->city->country->name}}</span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Member Since</span>
                                        </div>

                                        <div class="right">
                                            <span>{{\Illuminate\Support\Carbon::parse($service->user->created_at)->format('Y')}}</span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Avg. Response Time</span>
                                        </div>

                                        <div class="right">
                                            <span>1 Hrs</span>
                                        </div>
                                    </li>
                                </ul>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>

                                <div class="contact-link mx-auto">
                                    <a class="text-center" href="#">Contact me</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .description -->

    <section class="seller"><!-- start .seller -->
        <div class="carousel">
            <div class="container">
                <div class="seller-area mx-auto mb-4">
                    <div class="title">
                        <h4>Recommended for you</h4>
                    </div>

                    <div class="seller-container px-5">
                        <div class="owl-carousel owl-theme seller-carousel ">
                            <div class="seller-item">
                                <div id="seller1-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller2-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller3-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller4-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller5-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seller-area mx-auto mb-4">
                    <div class="seller-container px-5">
                        <div class="owl-carousel owl-theme seller-carousel ">
                            <div class="seller-item">
                                <div id="seller6-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262" >
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller7-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller8-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller9-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>

                            <div class="seller-item">
                                <div id="seller10-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">
                                            <div class="info">
                                                <h5>Freelancer username</h5>
                                                <p>Top Rated Seller</p>
                                            </div>

                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>Message from freelancer or description of the gig!</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .seller -->

    @endsection
