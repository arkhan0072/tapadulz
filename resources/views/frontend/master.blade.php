<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tapadul | Home</title>
    <link rel="shortcut icon" href="{{asset('public/images/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,300i,400,500,600,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('public/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('public/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/responsive.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />

    @yield('css-styles')
    <style>
        .user img {
        border-radius: 50%;
        }
    </style>

    <!-- Icons -->
{{--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">--}}
    <link rel="stylesheet" href="{{asset('public/dist/themes/fontawesome-stars.css')}}">
{{--    <link rel="stylesheet" href="{{asset('public/dist/themes/css-stars.css')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('public/dist/themes/bootstrap-stars.css')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('public/dist/themes/fontawesome-stars-o.css')}}">--}}

</head>

<body>
<div id="wrapper"><!-- start wrapper -->
    <header class="bg-white">
        <div class="container">
            <div class="header-area mx-auto"><!-- start .header-area -->
                <div class="header-top clearfix">
                    <div class="logo-area clearfix">
                        <div class="logo">

                            @if(\Illuminate\Support\Facades\Auth::user() && \Illuminate\Support\Facades\Auth::user()->user_level==1)
                            <a href="{{route('ahome')}}">
                                <figure>
                                    <img src="{{asset('public/images/logo.png')}}" alt="" width="297" height="159">
                                </figure>
                            </a>
                                @elseif(\Illuminate\Support\Facades\Auth::user() && \Illuminate\Support\Facades\Auth::user()->user_level==0)

                                <a href="{{route('uhome')}}">
                                    <figure>
                                        <img src="{{asset('public/images/logo.png')}}" alt="" width="297" height="159">
                                    </figure>
                                </a>

                                @else
                                <a href="{{route('index1')}}">
                                    <figure>
                                        <img src="{{asset('public/images/logo.png')}}" alt="" width="297" height="159">
                                    </figure>
                                </a>
                            @endif
                        </div>

                        <form action="{{ route('search') }}" class="clearfix">
                            <div class="form-group">
                                <input type="submit" value="" class="search border-0 px-0">

                                <input type="text"  class="form-control py-0 key1" placeholder="Find services you need!">
                            </div>

                            <button class="btn py-0" id="cc">Search</button>
                        </form>
                    </div>
                    @if(\Illuminate\Support\Facades\Auth::user())
                    <div class="header-top-right">

                        <div class="clearfix">
                            <ul class="nav">
                                <li><a class="px-0 py-0" href="#">Home</a></li>
                                <li><a class="px-0 py-0" href="#">Community</a></li>
                                <li><a class="px-0 py-0" href="{{route('messages')}}">Messages</a></li>
                                <li class="dropdown">
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Orders</span></a>

                                                                    <div class="dropdown-menu">
                                                                        <a class="dropdown-item"  href="{{route('offers')}}">Offers</a>
                                                                        <a class="dropdown-item" href="{{route('offersReceived')}}">Orders</a>
                                                                    </div>
                                                                </li>
                            </ul>
                        </div>


                        <div class="user mb-0 ml-auto d-none d-lg-block clearfix">
                            <div class="user-name">
                                <h5><a href="{{route('profile',\Illuminate\Support\Facades\Auth::user()->username)}}">{{\Illuminate\Support\Facades\Auth::user()->name}}</a></h5>
                                <h6><a class="px-0 py-0" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></h6>
                            </div>
                @if(\Illuminate\Support\Facades\Auth::user()->load('picture')->picture)
                            <figure>
                                <img src="{{asset('public/images/users/')}}/{{\Illuminate\Support\Facades\Auth::user()->load('picture')->picture->name ? \Illuminate\Support\Facades\Auth::user()->load('picture')->picture->name:'user2.png'}}" alt="" width="87" height="86">
                            </figure>

                        @else
                            <figure>
                                <img src="{{asset('public/images/users/default.png')}}" alt="" width="87" height="86">
                            </figure>
                            @endif
                        </div>
                        @else
                    {{--Login & Signup button goes here --}}
                            <div class="clearfix">
                                <ul class="nav float-right" style="padding: 5px!important;">
                                    <li style="padding-right:2px!important;"><a href="{{route('login')}}" class="btn btn-primary" style="background-color: #17839e!important;border-color:#17839e!important;">Login</a></li>
                                    <li style="padding-left:2px!important;"><a href="{{route('register')}}" class="btn btn-primary" style="background-color: #17839e!important;border-color:#17839e!important;">Signup</a></li>
                                </ul>
                            </div>

                    @endif
                    </div>
                </div>
                <nav class="navbar navbar-expand-xl navbar-light px-0 py-0">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item design">
                                <a class="nav-link text-center" href="#">Graphics & Design</a>
                            </li>

                            <li class="nav-item marketing">
                                <a class="nav-link text-center" href="#">Digital Marketing</a>
                            </li>

                            <li class="nav-item writing">
                                <a class="nav-link text-center" href="#">Writing & Translations</a>
                            </li>

                            <li class="nav-item video">
                                <a class="nav-link text-center" href="#">Video & Animarions</a>
                            </li>

                            <li class="nav-item music">
                                <a class="nav-link text-center" href="#">Music & Audio</a>
                            </li>

                            <li class="nav-item tech">
                                <a class="nav-link text-center" href="#">Programmign & Tech</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-center" href="#">Business</a>
                            </li>

                            <li class="nav-item lifestyle">
                                <a class="nav-link text-center" href="#">Lifestyle</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div><!-- //end .header-area -->
        </div>
        @yield('header')
    </header>
    @yield('content')
    <footer>
        <div class="container">
            <div class="footer-area"><!-- start .footer-area -->
                <div class="row">
                    <div class="col-sm-7">
                        <div class="footer-logo clearfix">
                            <figure>
                                <img src="{{asset('public/images/footer-logo.png')}}" alt="" width="206" height="110">
                            </figure>

                            <div class="copyright">
                                <p>All Rights Reserveed, 2020</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="social clearfix">
                            <ul class="nav">
                                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- //end .footer-area -->
        </div>
    </footer>
</div>
    <script src="{{asset('public/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/js/jquery.flexslider.js')}}"></script>
    <script src="{{asset('public/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('public/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('public/js/custom.js')}}"></script>
<script>
    $(document).ready(function () {


        $('.idpass').click(function (e) {
            e.preventDefault();
            var id=$(this).data('id');
            var service_needed=$(this).data('service_needed');
            var service_provided=$(this).data('service_provided');
            var token=$('#token').val();
            var reciever_id=$(this).data('reciever_id');
            // alert(token);
            $.ajax({
                url:'{{route('offers.store')}}',
                type:'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id :id,
                    service_needed :service_needed,
                    service_provided :service_provided,
                    reciever_id :reciever_id
                },
                success:function (data) {
                    console.log(data);
                    location.reload();
                }
            });
        });
    })
</script>
@yield('scripts')
@yield('js-scripts')
<link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>
<script src="https://cdn.tiny.cloud/1/4wopu0ubhg9ac9duwmvlxw1o08978v41htqeo9f56c2116lv/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src='https://cdn.tiny.cloud/1/4wopu0ubhg9ac9duwmvlxw1o08978v41htqeo9f56c2116lv/tinymce/5/tinymce.min.js' referrerpolicy="origin">
</script>
{{--<script>--}}
{{--    tinymce.init({--}}
{{--        selector: '#message-body'--}}
{{--    });--}}
{{--</script>--}}
<script>
    $(document).ready(function () {
        $('#category').select2();
        // $('.category').select2();
        // $('.amount').select2();
        $('#subcategory').select2();
        $('#country').select2();
        $('#city').select2();
        $('#duration').select2();
        $('#category').change(function () {
            var getId=$(this).val();
            $.ajax({
                url:"{{route('getsubcat')}}",
                type:'get',
                data:{
                    id:getId
                },
                success: function (data) {
                    $('#subcategory').html(data);
                }
            })
        })
        $('#country').change(function () {
            var getId=$(this).val();
            $.ajax({
                url:"{{route('getsubcities')}}",
                type:'get',
                data:{
                    id:getId
                },
                success: function (data) {
                    $('#city').html(data);
                }
            })
        })

    })
    $(document).ready(function () {
        $('#cc').click(function (e) {
            e.preventDefault();
            var key=$('.key1').val();
            window.location="{{route('search')}}?key="+key;
        })
        });
</script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script src="{{asset('public/dist/jquery.barrating.min.js')}}"></script>
@yield('scriptz')
</body>
</html>
