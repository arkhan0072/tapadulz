@extends('frontend.master')
@section('content')
    <section class="slogan"><!-- start slogan -->
        <div class="container">
            <div class="slogan-area mx-auto">
                <h2 class="text-center text-white">Contact us</h2>
                <p class="text-center text-white">Any slogan or text here</p>
            </div>
        </div>
    </section><!-- //end .slogan -->

    <section class="hero mt-0"><!-- start .hero -->
        <figure>
            <img src="{{asset('public/images/hero-support.png')}}" alt="" width="1920" height="1264">
        </figure>

        <div class="hero-area">
            <div class="vertical-column">
                <div class="vertical-cell">
                    <div class="container">
                        <div class="hero-info mx-auto">
                            <h1 class="text-center text-white">We’d love <span>to help!</span></h1>

                            <div class="button-area clearfix">
                                <div class="call-us link bg-white">
                                    <a class="text-center" href="#">Call us</a>
                                </div>

                                <div class="email-us link">
                                    <a class="text-white text-center" href="#">Email Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .hero -->

    <section class="support"><!-- start .support -->
        <div class="container">
            <div class="support-area">
                <div class="row">
                    <div class="col-md-6 col-lg-5">
                        <div class="support-info">
                            <h3>Our Story</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>

                            <div class="social">
                                <ul class="nav">
                                    <li><a class="text-center text-white" href=""><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="text-center text-white" href=""><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="text-center text-white" href=""><i class="fa fa-linkedin"></i></a></li>
                                    <li><a class="text-center text-white" href=""><i class="fa fa-pinterest"></i></a></li>
                                    <li><a class="text-center text-white" href=""><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-5">
                        <div class="contact-form">
                            <h3>Send an Email</h3>

                            <form>
                                <div class="form-group clearfix">
                                    <label>Name</label>
                                    <input type="text" placeholder="Type your text here!">
                                </div>

                                <div class="form-group clearfix">
                                    <label>Email Address</label>
                                    <input type="email" placeholder="Type your text here!">
                                </div>

                                <div class="form-group clearfix">
                                    <label>Contact Number</label>
                                    <input type="tel" placeholder="Type your text here!">
                                </div>

                                <div class="form-group mt-3 clearfix">
                                    <label>Message</label>
                                    <textarea class="form-control" placeholder="Type your text here!"></textarea>
                                </div>

                                <button class="text-white btn ml-auto py-0">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .support -->

    <section class="special-links"><!-- start special-links -->
        <div class="container">
            <div class="special-links-area mx-auto">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item category">
                            <h4>Catageories</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item about">
                            <h4>About</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item support">
                            <h4>Support</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="special-links-item community">
                            <h4>Community</h4>
                            <ul>
                                <li><a href="#">Graphics & Design</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Writing & Translations</a></li>
                                <li><a href="#">Video & Animations</a></li>
                                <li><a href="#">Music & Audio</a></li>
                                <li><a href="#">Programming & Tech</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Lifestyle</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .special-links -->
@endsection

