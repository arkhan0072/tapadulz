<section id="home-slider" class="flexslider">
    <ul class="slides">
        <li>
            <div class="banner"><!-- start .banner -->
                <div class="container">
                    <div class="banner-area mx-auto">
                        <div class="vertical-column">
                            <div class="vertical-cell">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="banner-info">
                                            <h1 class="text-white">Sit back and relax</h1>
                                            <p class="text-white">we got you covered!</p>

                                            <div class="button-area clearfix">
                                                <div class="banner-link border-0">
                                                    <a class="text-center" href="">Button #1</a>
                                                </div>

                                                <div class="banner-link">
                                                    <a class="text-center" href="">Button #2</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <figure>
                                            <img src="{{asset('public/images/banner-img.png')}}" alt="" width="908" height="505">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- //end .banner -->
        </li>

        <li>
            <div class="banner bg2"><!-- start .banner -->
                <div class="container">
                    <div class="banner-area mx-auto">
                        <div class="vertical-column">
                            <div class="vertical-cell">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="banner-info">
                                            <h1 class="text-white">Sit back and relax</h1>
                                            <p class="text-white">we got you covered!</p>

                                            <div class="button-area clearfix">
                                                <div class="banner-link border-0">
                                                    <a class="text-center" href="">Button #1</a>
                                                </div>

                                                <div class="banner-link">
                                                    <a class="text-center" href="">Button #2</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <figure>
                                            <img src="{{asset('public/images/banner-img.png')}}" alt="" width="908" height="505">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- //end .banner -->
        </li>

        <li>
            <div class="banner"><!-- start .banner -->
                <div class="container">
                    <div class="banner-area mx-auto">
                        <div class="vertical-column">
                            <div class="vertical-cell">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="banner-info">
                                            <h1 class="text-white">Sit back and relax</h1>
                                            <p class="text-white">we got you covered!</p>

                                            <div class="button-area clearfix">
                                                <div class="banner-link border-0">
                                                    <a class="text-center" href="">Button #1</a>
                                                </div>

                                                <div class="banner-link">
                                                    <a class="text-center" href="">Button #2</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <figure>
                                            <img src="{{asset('public/images/banner-img.png')}}" alt="" width="908" height="505">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- //end .banner -->
        </li>

        <li>
            <div class="banner bg2"><!-- start .banner -->
                <div class="container">
                    <div class="banner-area mx-auto">
                        <div class="vertical-column">
                            <div class="vertical-cell">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="banner-info">
                                            <h1 class="text-white">Sit back and relax</h1>
                                            <p class="text-white">we got you covered!</p>

                                            <div class="button-area clearfix">
                                                <div class="banner-link border-0">
                                                    <a class="text-center" href="">Button #1</a>
                                                </div>

                                                <div class="banner-link">
                                                    <a class="text-center" href="">Button #2</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <figure>
                                            <img src="{{asset('public/images/banner-img.png')}}" alt="" width="908" height="505">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- //end .banner -->
        </li>
    </ul>
</section>
