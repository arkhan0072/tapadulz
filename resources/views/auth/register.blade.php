@extends('frontend.master')

@section('content')

    <section class="banner bg-graylight"><!-- start .banner -->
        <div class="container">
            <div class="banner-area mx-auto">
                <div class="row">
                    <div class="col-md-6">
                        <div class="banner-info login-signup">
                            <h1 class="text-white">We got the best talent</h1>
                            <p class="text-white">Hire freelancers now!</p>

                            <div class="button-area clearfix">
                                <div class="banner-link bg border-0">
                                    <a class="text-center text-white" href="">Get Started</a>
                                </div>

                                <div class="banner-link shadow">
                                    <a class="text-center" href="">Free Trail</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <figure>
                            <img src="{{asset('public/images/banner-img2.png')}}" alt="" width="646" height="644" >
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .banner -->

    <section class="registration"><!-- start .registration -->
        <div class="container">
            <div class="registration-area mx-auto">
                <div class="row">
                    <div class="col-md-7 col-lg-8">
                        <div class="explore">
                            <div class="container">
                                <div class="heading px-0">
                                    <h3>Categories</h3>
                                </div>

                                <div class="explore-area mx-auto">
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/graphic.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Graphics & Design</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/marketing.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Digital Marketing</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/writing.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Writing & Translations</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/video.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Video & Animarions</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/music.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Music & Audio</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/tech.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Programmign & Tech</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/business.png')}}" alt="" width="151" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Business</h4>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <a href="#">
                                                <div class="explore-item">
                                                    <figure class="mx-auto">
                                                        <img src="{{asset('public/images/lifestyle.png')}}" alt="" width="113" height="144">
                                                    </figure>

                                                    <h4 class="text-center">Lifestyle</h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="freelancer-rate">
                            <div class="row">
                                <div class="col-sm-6 col-lg-4">
                                    <a href="#">
                                        <div class="item">
                                            <figure>
                                                <img src="{{asset('public/images/freelancer1.png')}}" alt="" width="290" height="375">
                                            </figure>
                                            <h4>Freelancer name</h4>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <a href="#">
                                        <div class="item">
                                            <figure>
                                                <img src="{{asset('public/images/freelancer2.png')}}" alt="" width="290" height="375">
                                            </figure>
                                            <h4>Freelancer name</h4>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <a href="#">
                                        <div class="item">
                                            <figure>
                                                <img src="{{asset('public/images/freelancer3.png')}}" alt="" width="290" height="375">
                                            </figure>
                                            <h4>Freelancer name</h4>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <a href="#">
                                        <div class="item">
                                            <figure>
                                                <img src="{{asset('public/images/freelancer4.png')}}" alt="" width="290" height="375">
                                            </figure>
                                            <h4>Freelancer name</h4>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <a href="#">
                                        <div class="item">
                                            <figure>
                                                <img src="{{asset('public/images/freelancer5.png')}}" alt="" width="290" height="375">
                                            </figure>
                                            <h4>Freelancer name</h4>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <a href="#">
                                        <div class="item">
                                            <figure>
                                                <img src="{{asset('public/images/freelancer6.png')}}" alt="" width="290" height="375">
                                            </figure>
                                            <h4>Freelancer name</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5 col-lg-4">
                        <div class="registration-form">
                                <form method="POST" class="mx-auto" action="{{ route('register') }}">
                                    @csrf

                                    <div class="form-group">
                                        <label for="fullname">Full Name</label>
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror text-center" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror text-center" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="emailaddress">Email address</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror text-center" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror text-center" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Confirm Password</label>

                                            <input id="password-confirm" type="password" class="form-control text-center" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-signup">
                                            <label class="custom-control-label" for="checkbox-signup">I accept <a href="javascript: void(0);" class="text-dark">Terms and Conditions</a></label>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-success btn-block" type="submit"> Sign Up </button>
                                    </div>

                                </form>
                        </div>

                        <div class="sign-up">
                            <h5 class="text-center">Not a member?</h5>
                            <div class="sign-up-link mx-auto">
                                <a class="text-center text-white px-3" href="{{route('register')}}">Sign up</a>
                            </div><br>
                            <a href="{{ route('password.request') }}"><h5 class="text-center">Forgot password?</h5></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .registration -->


    <section class="policy bg-blue"><!-- start policy -->
        <div class="policy-area">
            <div class="container">
                <div class="policy-info mx-auto">
                    <h3 class="text-center text-white">Find Freelance Services For Your Business Today</h3>
                    <p class="text-center text-white">We've got you covered for all your business needs</p>

                    <div class="policy-area-link mx-auto">
                        <a class="text-center" href="#">Get Started!</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .policy -->
@endsection
