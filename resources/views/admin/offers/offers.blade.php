@extends('frontend.master')
@section('content')
    <section class="order"><!-- start .order -->
        <div class="container">
            <div class="order-area mx-auto">
                <div class="show_data">

                </div>
            </div>
        </div>
    </section><!-- //end .order -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $.ajax({
                url:"{{ route('orderajax') }}",
                type:'get',
                success:function (data) {
                    $('.show_data').html(data);
                },
            });
        });
    </script>
@endsection
