@extends('frontend.master')
@section('content')
    <section class="order"><!-- start .order -->
        <div class="container">
            <div class="order-area mx-auto">
{{--                <div class="description-area mx-auto">--}}
{{--                    <h3>Description of the Order goes here!</h3>--}}
{{--                    <div class="gig-info clearfix">--}}
{{--                        <div class="user-name clearfix">--}}
{{--                            <div class="gig-user clearfix">--}}
{{--                                <figure>--}}
{{--                                    <img src="images/user.png" alt="" width="67" height="66">--}}
{{--                                </figure>--}}

{{--                                <div class="name clearfix">--}}
{{--                                    <h5>Freelancer username</h5>--}}
{{--                                    <p>Top Rated Seller</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="rating clearfix">--}}
{{--                                <ul class="nav">--}}
{{--                                    <li><i class="fa fa-star"></i></li>--}}
{{--                                    <li><i class="fa fa-star"></i></li>--}}
{{--                                    <li><i class="fa fa-star"></i></li>--}}
{{--                                    <li><i class="fa fa-star"></i></li>--}}
{{--                                    <li><i class="fa fa-star"></i></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="info">--}}
{{--                            <h4>Other details of the Order goes here</h4>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="show_data">

                </div>
            </div>
        </div>
    </section><!-- //end .order -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $.ajax({
                url:"{{ route('ajaxOffers') }}",
                type:'get',
                success:function (data) {
                    $('.show_data').html(data);
                },
            });
            });
            $(document).click(function () {
            $('.approved').click(function (e) {
                e.preventDefault();
                var offer_id=$(this).data('id');
                var sender_id=$(this).data('sender_id');
                var receiver_id=$(this).data('receiver_id');

                $.ajax({
                    url:'{{route('Approved')}}',
                    type:'post',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        offer_id :offer_id,
                        sender_id :sender_id,
                        receiver_id :receiver_id
                    },
                    success:function (data) {
                        console.log(data.username);
                        $.ajax({
                            url:"{{ route('ajaxOffers') }}",
                            type:'get',
                            success:function (data) {
                                $('.show_data').html(data);
                            },
                        });
                    }
                });
            });
            $('.rejects').click(function (e) {
                e.preventDefault();
                var offer_id=$(this).data('id');
                var sender_id=$(this).data('sender_id');
                var receiver_id=$(this).data('receiver_id');
                // alert(token);
                $.ajax({
                    url:'{{route('Reject')}}',
                    type:'post',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        offer_id :offer_id,
                        sender_id :sender_id,
                        receiver_id :receiver_id
                    },
                    success:function (data) {
                        console.log(data);
                        $.ajax({
                            url:"{{ route('ajaxOffers') }}",
                            type:'get',
                            success:function (data) {
                                $('.show_data').html(data);
                            },
                        });
                        // location.reload();
                    }
                });
            });

       });
    </script>
@endsection
