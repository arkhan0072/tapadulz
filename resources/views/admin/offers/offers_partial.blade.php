<Style>
    a.text-white.cancel.btn.btn-danger{
        line-height: 40px;
    }
    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
    }
    .br-theme-fontawesome-stars .br-widget a.br-selected:after {
        color:#F8DE08!important;
    }

</Style>
@foreach($orders as $key=>$order)
{{--    {{dd($order->provided->status)}}--}}
    @if($order->provided)
<div class="row">
<div class="col-md-12 col-lg-12">
    <div class="order-item">
        <div class="order-desc">
            <div class="clearfix">
                <div class="title">
                    <h4>{{$order->provided->service->name}}</h4>
                </div>

                <div class="status">
                    @if($order->provided->status==2)
                        <p>Under Process</p>
                    @elseif($order->provided->status==3)
                        <p>Delivered</p>
                    @elseif($order->provided->status==4)
                        <p>Accepted</p>
                    @elseif($order->provided->status==5)
                        <p>Completed</p>
                    @endif
                </div>
            </div>

            <div class="border"></div>
            <p>{!! html_entity_decode(str_limit($order->provided->service->details,50,'...')) !!}</p>
            <div class="clearfix">
                <div class="action clearfix">
                    @if($order->provided->status!=4)
                    <div class="accept ml-4">
                        <a data-toggle="modal" data-target="#myModal{{$order->provided->id}}"  data-provided_id="{{$order->provided->id}}" class="deliver text-white">Deliver</a>
                            <div class="modal fade" id="myModal{{$order->provided->id}}" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Modal Header</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{route('orderitemfile.store')}}" enctype="multipart/form-data" method="post">
                                            @csrf
                                            <input type="hidden" name="provided_id" value="" class="provided_id">
                                            <textarea class="form-control" name="message" id="message" placeholder="Enter your text Message"></textarea>
                                            <input type="file" name="delivery[]" multiple>
                                            <input type="submit" value="Deliver">
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endif

                    <div class="accept ml-4">
                        <a class="text-white" href="{{route('history',$order->id)}}">Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--@if($order->status==5)--}}
{{--    <div class="reviews-box">--}}
{{--        <div class="top clearfix">--}}
{{--            <div class="top-info mb-0 clearfix">--}}
{{--                <div class="name px-0">--}}
{{--                    <h5>Give A Review</h5>--}}
{{--                </div>--}}

{{--                <div class="star mb-3">--}}
{{--                    <ul>--}}
{{--                        <li class="d-flex">--}}
{{--                            <span class="pr-3">write something about</span>--}}
{{--                            <select class="stars stars-example-fontawesome example-fontawesome" name="q1" autocomplete="off">--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                            </select>--}}
{{--                        </li>--}}
{{--                        <li class="d-flex">--}}
{{--                            <span class="pr-3">write something about</span>--}}
{{--                            <select class="stars stars-example-fontawesome example-fontawesome" name="q2" autocomplete="off">--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                            </select>--}}
{{--                        </li>--}}

{{--                        <li class="d-flex">--}}
{{--                            <span class="pr-3">write something about</span>--}}
{{--                            <select class="stars stars-example-fontawesome example-fontawesome" name="q3" autocomplete="off">--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                            </select>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="timeline mb-3">--}}
{{--                <p>2 days ago</p>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <form>--}}
{{--            <div class="form-group">--}}
{{--                <textarea class="form-control"></textarea>--}}
{{--            </div>--}}

{{--            <button class="btn text-white" type="submit">Submit</button>--}}
{{--        </form>--}}
{{--    </div>--}}

{{--    @endif--}}
</div>

{{--<div class="col-md-12 col-lg-4 sidebar">--}}
{{--    <div class="item">--}}
{{--        <div class="row mx-0">--}}
{{--            <div class="col-7 px-0">--}}
{{--                <h4 class="mb-2">Order Info</h4>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="contact-form">--}}
{{--            <form class="bg-white">--}}
{{--                <div class="form-group clearfix">--}}
{{--                    <label class="text-left">Order Id</label>--}}
{{--                    <input class="bg-gray"  value="{{"X-1081-0".$order->id}}" type="text" readonly>--}}
{{--                </div>--}}
{{--                <div class="form-group clearfix">--}}

{{--                    <label class="text-left">Status {{$order->provided->service->user->username}}</label>--}}

{{--                    @if($order->provided->status==2)--}}
{{--                    <input class="bg-gray" type="text" value="Under Process" readonly>--}}
{{--                        @elseif($order->provided->status==3)--}}
{{--                            <input class="bg-gray" type="text" value="Delivered" readonly>--}}
{{--                        @elseif($order->provided->status==4)--}}
{{--                            <input class="bg-gray" type="text" value="Accepted" readonly>--}}
{{--                        @elseif($order->provided->status==5)--}}
{{--                            <input class="bg-gray" type="text" value="Completed" readonly>--}}
{{--                        @endif--}}
{{--                </div>--}}

{{--                <div class="form-group clearfix">--}}
{{--                    <label class="text-left">Status {{$order->needed->service->user->username}}</label>--}}
{{--                    @if($order->needed->status==2)--}}
{{--                        <input class="bg-gray" type="text" value="Under Process" readonly>--}}
{{--                    @elseif($order->needed->status==3)--}}
{{--                        <input class="bg-gray" type="text" value="Delivered" readonly>--}}
{{--                    @elseif($order->needed->status==4)--}}
{{--                        <input class="bg-gray" type="text" value="Accepted" readonly>--}}
{{--                    @elseif($order->needed->status==5)--}}
{{--                        <input class="bg-gray" value="Completed" type="text" readonly>--}}
{{--                    @endif--}}
{{--                </div>--}}

{{--                <div class="form-group clearfix">--}}
{{--                    <label class="text-left">Over All Status</label>--}}
{{--                    <input class="bg-gray" value="{{$order->status == 5 ? 'Completed':'Under Process'}}" type="text" readonly>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}

{{--        <div class="exchange-link mx-auto">--}}
{{--            <a class="text-center text-white px-5" href="{{route('messages')}}">Chat</a>--}}
{{--        </div>--}}
{{--        <div class="exchange-link mx-auto mt-2">--}}
{{--            <a class="text-center text-white px-5" href="{{route('history',$order->id)}}">Details</a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
</div>
@endif
@endforeach
                    <script>
                        $(document).ready(function () {
                            $('.deliver').click(function (e) {
                                var provided_id=$(this).data('provided_id');
                                $('.provided_id').val(provided_id);
                            })
                        });
                        $(document).ready(function () {
                            $('.Accpet').click(function (e) {
                                e.preventDefault();
                                var provided_id=$(this).data('provided_id');
                                $.ajax({
                                    url:"{{route('accepted')}}",
                                    type:'get',
                                    data:{
                                        provided_id:provided_id,
                                    },
                                    success:function (data) {
                                        console.log(data);
                                    }
                                })
                            });
                            $('.Reject').click(function (e) {
                                e.preventDefault();
                                var delivery_id=$(this).data('delivery_id');
                                var provided_id=$(this).data('provided_id');
                                var service_id=$(this).data('service_id');
                                $.ajax({
                                    url:"{{route('DeliveryReject')}}",
                                    type:'get',
                                    data:{
                                        provided_id:provided_id,
                                    },
                                    success:function (data) {
                                        console.log(data);
                                    }
                                })
                            });
                            });


</script>
