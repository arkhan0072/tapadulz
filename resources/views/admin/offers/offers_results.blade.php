
@foreach($offers as $offer)
    <div class="row">
        <div class="col-md-12 col-lg-8">
            <div class="order-item">
                <div class="order-desc">
                    <div class="clearfix">
                        <div class="title">
                            <h4><a href="{{route('viewgig',$offer->needed->service->slug)}}">{{$offer->needed->service->name}}</a></h4>
                        </div>
                        <div class="status">
                            @if($offer->needed->status==1)
                                <p>Pending</p>
                            @elseif($offer->status==2)
                                <p>Under Process</p>
                            @elseif($offer->status==3)
                                <p>Delivered</p>
                            @elseif($offer->status==4)
                                <p>Accepted</p>
                            @elseif($offer->status==5)
                                <p>Completed</p>
                            @endif
                        </div>
                    </div>

                    <div class="border"></div>
                    <p>{!! html_entity_decode(str_limit($offer->needed->service->details,50,'...')) !!}</p>
                    <div class="clearfix">
                        <div class="action clearfix">
                            @if($offer->status==1)
                                <div class="accept ml-4">
                                    <a href="#" class="text-white approved" title="Accept" data-sender_id="{{$offer->sender_id}}" data-receiver_id="{{$offer->receiver_id}}" data-offer_needed="{{$offer->needed->id}}" data-offer_provided="{{$offer->provided->id}}" data-id="{{$offer->needed->offer_id}}" >Accept</a>
                                </div>
                            @else
                                <div class="accept ml-4">
                                    <a href="{{route("Deliver")}}" data-sender_id="{{$offer->sender_id}}" data-receiver_id="{{$offer->receiver_id}}" data-id="{{$offer->id}}" class="deliver text-white">Deliver</a>
                                </div>
                            @endif
                            @if($offer->status==6)
                                <div class="reject ml-4">
                                    <a href="#" class="text-white cancel btn btn-danger" title="Cancel" data-sender_id="{{$offer->sender_id}}" data-receiver_id="{{$offer->receiver_id}}" data-id="{{$offer->id}}" >Cancel</a>
                                </div>
                            @else
                                <div class="reject ml-4">
                                    <a href="#" data-sender_id="{{$offer->sender_id}}" data-receiver_id="{{$offer->receiver_id}}" data-id="{{$offer->id}}" class="rejects text-white">Reject</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
<script>
    $('.approved').click(function (e) {
        e.preventDefault();
        var offer_id=$(this).data('id');
        var sender_id=$(this).data('sender_id');
        var receiver_id=$(this).data('receiver_id');
        var offer_needed=$(this).data('offer_needed');
        var offer_provided=$(this).data('offer_provided');

        $.ajax({
            url:'{{route('Approved')}}',
            type:'post',
            data: {
                "_token": "{{ csrf_token() }}",
                offer_id :offer_id,
                sender_id :sender_id,
                receiver_id :receiver_id,
                offer_needed :offer_needed,
                offer_provided :offer_provided
            },
            success:function (data) {
                console.log(data.username);
                $.ajax({
                    url:"{{ route('orderajax') }}",
                    type:'get',
                    success:function (data) {
                        $('.show_data').html(data);
                        window.location.href='{{route('offersReceived')}}';
                    },
                });
            }
        });
    });
    $('.rejects').click(function (e) {
        e.preventDefault();
        var offer_id=$(this).data('id');
        var sender_id=$(this).data('sender_id');
        var receiver_id=$(this).data('receiver_id');
        // alert(token);
        $.ajax({
            url:'{{route('Reject')}}',
            type:'post',
            data: {
                "_token": "{{ csrf_token() }}",
                offer_id :offer_id,
                sender_id :sender_id,
                receiver_id :receiver_id
            },
            success:function (data) {
                console.log(data);
                $.ajax({
                    url:"{{ route('orderajax') }}",
                    type:'get',
                    success:function (data) {
                        $('.show_data').html(data);
                    },
                });
                // location.reload();
            }
        });
    });
</script>
