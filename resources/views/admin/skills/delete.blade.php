<a href="#" onclick="frmdlt{{$skill->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$skill->id}}" action="{{ route('skills.destroy', $skill->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
