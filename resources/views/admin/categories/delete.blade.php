<a href="#" onclick="frmdlt{{$category->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$category->id}}" action="{{ route('categories.destroy', $category->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
