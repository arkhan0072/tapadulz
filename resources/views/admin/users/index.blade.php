@extends('layouts.master')

@section('css')
    <!-- third party css -->
    <link href="{{ URL::asset('public/ubold/assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                        <li class="breadcrumb-item active">Datatables</li>
                    </ol>
                </div>
                <h4 class="page-title">Datatables</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Services</h4>
                    <p class="text-muted font-13 mb-4">
                    </p>

                    <table id="service-datatable" class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Description</th>
                            <th>City</th>
                            <th>Country</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('script')

    <!-- third party js -->
    <script src="{{ URL::asset('public/ubold/assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('public/ubold/assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->

    <!-- Datatables init -->
    <script src="{{ URL::asset('public/ubold/assets/js/pages/datatables.init.js')}}"></script>
    <script>
        $(document).ready(function () {


            $('#service-datatable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('Userajax') }}",
                buttons: [
                    {{--{--}}
                        {{--    text: 'Add Hotel',--}}
                        {{--    action: '{{route('hotels.create')}}',--}}
                        {{--    className:'btn btn-primary'--}}
                        {{--    },--}}
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id',title:'id'},
                    {data: 'name', name: 'name',title:'Name'},
                    {data: 'email', name: 'email',title:'Email'},
                    {data: 'description', name: 'description',title:'Description'},
                    {data: 'status', name: 'status',title:'Status'},
                    // {data: 'subcategory.name', name: 'subcategory.name',title:'SubCategory'},
                    // {data: 'duration', name: 'duration',title:'Duration'},
                    {data: 'city.name', name: 'city.name',title:'City'},
                    {data: 'city.country.name', name: 'city.country.name',title:'Country'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
