@extends('frontend.master')

@section('content')
    <section class="description"><!-- start .description -->
        <div class="container">
            <div class="main mx-auto">
                <div class="description-area mx-auto"></div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="sidebar">
                            @if(\Illuminate\Support\Facades\Auth::user())
                                <div class="item">
                                    <div class="exchange-link mx-auto">
                                        <a class="text-center text-white" href="{{route('services.create')}}">Add New Service</a>
                                    </div>
                                </div>
                            @endif

                            <div class="item bg-gray">
                                <h5 class="text-center">Do you have any other requirement?</h5>
                                <div class="quote-link mx-auto">
                                    <a class="text-white" href="#">Get a quote</a>
                                </div>
                            </div>

                            <div class="item">
                                <h4>About the seller</h4>

                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/user3.png')}}" alt="" width="193" height="190">
                                </figure>

                                <div class="user-name">
                                    <h5 class="text-center mb-0"><a href="{{route('profile',$user->username)}}">{{$user->name}}</a></h5>
                                    <p class="text-center">{{$user->username}}</p>
                                </div>

                                <div class="rating clearfix">
                                    <div class="clearfix">
                                        <ul class="mx-auto">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>

                                    <p class="text-center">5.0 of 160 reviews</p>
                                </div>

                                <ul>
                                    <li class="clearfix">
                                        <div class="left">
                                            <span>From</span>
                                        </div>

                                        <div class="right">
                                            <span>{{$user->city ? $user->city->country->name:''}}</span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Member Since</span>
                                        </div>

                                        <div class="right">
                                            <span>{{\Carbon\Carbon::parse($user->created_at)->format('Y')}}</span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Avg. Response Time</span>
                                        </div>

                                        <div class="right">
                                            <span>1 Hrs</span>
                                        </div>
                                    </li>
                                </ul>


                                <p>{{$user->description}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-8">
                        <div class="seller-area mx-auto mb-4">
                            <div class="title">
                                <h4>Gigs by Seller</h4>
                            </div>

                            <div class="row mx-0">
                                @foreach($user->serceives as $service)
                                <div class="col-md-6 px-0">

                                    <div class="seller-item">

                                            <div id="seller{{$service->id}}-slider" class="flexslider">
                                                <ul class="slides">
                                                    <li>
                                                        <figure>
                                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                                 height="262">
                                                        </figure>
                                                    </li>

                                                    <li>
                                                        <figure>
                                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                                 height="262">
                                                        </figure>
                                                    </li>

                                                    <li>
                                                        <figure>
                                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                                 height="262">
                                                        </figure>
                                                    </li>

                                                    <li>
                                                        <figure>
                                                            <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406"
                                                                 height="262">
                                                        </figure>
                                                    </li>
                                                </ul>
                                            </div>

                                        <div class="seller-info">
                                            <div class="user clearfix">
                                                <figure>
                                                    <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                                </figure>

                                                <div class="user-name clearfix">
                                                    <div class="info">
                                                        <h5><a href="{{route('profile',$user->username)}}">{{$user->username}}</a></h5>
                                                        <p>Top Rated Seller</p>
                                                    </div>

                                                    <a href="#">
                                                        <figure>
                                                            <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                        </figure>
                                                    </a>
                                                </div>
                                            </div>

                                            <h4><a href="{{route('viewgig',$service->slug)}}">{{str_limit(str_replace('&nbsp;',' ',$service->details),150,'...')}}</a></h4>

                                            <div class="price clearfix">
                                                <ul class="nav">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>

                                                <span>$1,000</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                @endforeach

                            </div>
                        </div>

                        <div class="reviews">
                            <div class="heading clearfix">
                                <div class="title">
                                    <h4>Reviews as Seller</h4>
                                </div>

                                <div class="link">
                                    <a href="#"><span>Most Recent</span></a>
                                </div>
                            </div>

                            <div class="review-box-wrap" id="boxscroll">
                                <div class="reviews-box pr-3">
                                    <div class="top clearfix">
                                        <div class="top-info clearfix">
                                            <figure>
                                                <img src="{{asset('public/images/review-img.png')}}" alt="" width="70" height="74">
                                            </figure>

                                            <div class="name">
                                                <h5>Salmn Altaf</h5>
                                                <span>Pakistan</span>
                                            </div>

                                            <div class="star">
                                                <ul class="nav">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="timeline">
                                            <p>2 days ago</p>
                                        </div>
                                    </div>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. </p>
                                </div>

                                <div class="reviews-box pr-3">
                                    <div class="top clearfix">
                                        <div class="top-info clearfix">
                                            <figure>
                                                <img src="{{asset('public/images/review-img.png')}}" alt="" width="70" height="74">
                                            </figure>

                                            <div class="name">
                                                <h5>Salmn Altaf</h5>
                                                <span>Pakistan</span>
                                            </div>

                                            <div class="star">
                                                <ul class="nav">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="timeline">
                                            <p>2 days ago</p>
                                        </div>
                                    </div>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. </p>
                                </div>

                                <div class="reviews-box pr-3">
                                    <div class="top clearfix">
                                        <div class="top-info clearfix">
                                            <figure>
                                                <img src="{{asset('public/images/review-img.png')}}" alt="" width="70" height="74">
                                            </figure>

                                            <div class="name">
                                                <h5>Salmn Altaf</h5>
                                                <span>Pakistan</span>
                                            </div>

                                            <div class="star">
                                                <ul class="nav">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="timeline">
                                            <p>2 days ago</p>
                                        </div>
                                    </div>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. </p>
                                </div>

                                <div class="reviews-box pr-3">
                                    <div class="top clearfix">
                                        <div class="top-info clearfix">
                                            <figure>
                                                <img src="{{asset('public/images/review-img.png')}}" alt="" width="70" height="74">
                                            </figure>

                                            <div class="name">
                                                <h5>Salmn Altaf</h5>
                                                <span>Pakistan</span>
                                            </div>

                                            <div class="star">
                                                <ul class="nav">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="timeline">
                                            <p>2 days ago</p>
                                        </div>
                                    </div>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- //end .description -->

    <section class="seller"><!-- start .seller -->
        <div class="carousel">
            <div class="container">
                <div class="seller-area mx-auto mb-4">
                    <div class="title">
                        <h4>Recommended for you</h4>
                    </div>

                    <div class="seller-container px-5">
                        <div class="owl-carousel owl-theme seller-carousel ">
                            @foreach($services as $serveic)
                            <div class="seller-item">
                                <div id="seller{{$serveic->id}}-slider" class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>

                                        <li>
                                            <figure>
                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">
                                            </figure>
                                        </li>
                                    </ul>
                                </div>

                                <div class="seller-info">
                                    <div class="user clearfix">
                                        <figure>
                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">
                                        </figure>

                                        <div class="user-name clearfix">

                                            <div class="info">

                                                <h5>{{$serveic->user->name}}</h5>

                                                <p>Top Rated Seller</p>
                                            </div>


                                            <a href="#">
                                                <figure>
                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <h4>{{$serveic->details}}</h4>

                                    <div class="price clearfix">
                                        <ul class="nav">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                        <span>$1,000</span>
                                    </div>
                                </div>
                            </div>
                                @endforeach


                        </div>
                    </div>
                </div>

{{--                <div class="seller-area mx-auto mb-4">--}}
{{--                    <div class="seller-container px-5">--}}
{{--                        <div class="owl-carousel owl-theme seller-carousel ">--}}
{{--                            <div class="seller-item">--}}
{{--                                <div id="seller6-slider" class="flexslider">--}}
{{--                                    <ul class="slides">--}}
{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="seller-info">--}}
{{--                                    <div class="user clearfix">--}}
{{--                                        <figure>--}}
{{--                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">--}}
{{--                                        </figure>--}}

{{--                                        <div class="user-name clearfix">--}}
{{--                                            <div class="info">--}}
{{--                                                <h5>Freelancer username</h5>--}}
{{--                                                <p>Top Rated Seller</p>--}}
{{--                                            </div>--}}

{{--                                            <a href="#">--}}
{{--                                                <figure>--}}
{{--                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">--}}
{{--                                                </figure>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4>Message from freelancer or description of the gig!</h4>--}}

{{--                                    <div class="price clearfix">--}}
{{--                                        <ul class="nav">--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                        </ul>--}}

{{--                                        <span>$1,000</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="seller-item">--}}
{{--                                <div id="seller7-slider" class="flexslider">--}}
{{--                                    <ul class="slides">--}}
{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img2.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="seller-info">--}}
{{--                                    <div class="user clearfix">--}}
{{--                                        <figure>--}}
{{--                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">--}}
{{--                                        </figure>--}}

{{--                                        <div class="user-name clearfix">--}}
{{--                                            <div class="info">--}}
{{--                                                <h5>Freelancer username</h5>--}}
{{--                                                <p>Top Rated Seller</p>--}}
{{--                                            </div>--}}

{{--                                            <a href="#">--}}
{{--                                                <figure>--}}
{{--                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">--}}
{{--                                                </figure>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4>Message from freelancer or description of the gig!</h4>--}}

{{--                                    <div class="price clearfix">--}}
{{--                                        <ul class="nav">--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                        </ul>--}}

{{--                                        <span>$1,000</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="seller-item">--}}
{{--                                <div id="seller8-slider" class="flexslider">--}}
{{--                                    <ul class="slides">--}}
{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img3.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="seller-info">--}}
{{--                                    <div class="user clearfix">--}}
{{--                                        <figure>--}}
{{--                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">--}}
{{--                                        </figure>--}}

{{--                                        <div class="user-name clearfix">--}}
{{--                                            <div class="info">--}}
{{--                                                <h5>Freelancer username</h5>--}}
{{--                                                <p>Top Rated Seller</p>--}}
{{--                                            </div>--}}

{{--                                            <a href="#">--}}
{{--                                                <figure>--}}
{{--                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">--}}
{{--                                                </figure>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4>Message from freelancer or description of the gig!</h4>--}}

{{--                                    <div class="price clearfix">--}}
{{--                                        <ul class="nav">--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                        </ul>--}}

{{--                                        <span>$1,000</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="seller-item">--}}
{{--                                <div id="seller9-slider" class="flexslider">--}}
{{--                                    <ul class="slides">--}}
{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img4.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="seller-info">--}}
{{--                                    <div class="user clearfix">--}}
{{--                                        <figure>--}}
{{--                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">--}}
{{--                                        </figure>--}}

{{--                                        <div class="user-name clearfix">--}}
{{--                                            <div class="info">--}}
{{--                                                <h5>Freelancer username</h5>--}}
{{--                                                <p>Top Rated Seller</p>--}}
{{--                                            </div>--}}

{{--                                            <a href="#">--}}
{{--                                                <figure>--}}
{{--                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">--}}
{{--                                                </figure>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4>Message from freelancer or description of the gig!</h4>--}}

{{--                                    <div class="price clearfix">--}}
{{--                                        <ul class="nav">--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                        </ul>--}}

{{--                                        <span>$1,000</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="seller-item">--}}
{{--                                <div id="seller10-slider" class="flexslider">--}}
{{--                                    <ul class="slides">--}}
{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}

{{--                                        <li>--}}
{{--                                            <figure>--}}
{{--                                                <img src="{{asset('public/images/seller-img1.png')}}" alt="" width="406" height="262">--}}
{{--                                            </figure>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="seller-info">--}}
{{--                                    <div class="user clearfix">--}}
{{--                                        <figure>--}}
{{--                                            <img src="{{asset('public/images/user.png')}}" alt="" width="67" height="66">--}}
{{--                                        </figure>--}}

{{--                                        <div class="user-name clearfix">--}}
{{--                                            <div class="info">--}}
{{--                                                <h5>Freelancer username</h5>--}}
{{--                                                <p>Top Rated Seller</p>--}}
{{--                                            </div>--}}

{{--                                            <a href="#">--}}
{{--                                                <figure>--}}
{{--                                                    <img src="{{asset('public/images/heart.png')}}" alt="" width="33" height="29">--}}
{{--                                                </figure>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4>Message from freelancer or description of the gig!</h4>--}}

{{--                                    <div class="price clearfix">--}}
{{--                                        <ul class="nav">--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                            <li><i class="fa fa-star"></i></li>--}}
{{--                                        </ul>--}}

{{--                                        <span>$1,000</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section><!-- //end .seller -->

@endsection
