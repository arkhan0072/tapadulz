@extends('layouts.master')

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Services</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
                <h4 class="page-title">Add New Service</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

    <div class="col-md-6 offset-3">
        <div class="x_panel">
            <div class="x_title">
                <h2>Settings</h2>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-info"></i> Success!</h4>
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="x_content">
                    <br>
                    <form method="post" action="{{route('settings.update')}}" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                        {{csrf_field()}}

                        <div class="form-group {{ $errors->has('PAYPAL_CLIENT_ID') ? 'has-error': ''}} ">
                            <label>PAYPAL_CLIENT_ID</label>
                            <input type="text" class="form-control" name="PAYPAL_CLIENT_ID"  value="{{old('PAYPAL_CLIENT_ID',Setting::get('PAYPAL_CLIENT_ID'))}}" >
                            @if($errors->has('PAYPAL_CLIENT_ID'))
                                <span class="help-block">
					<strong>{{ $errors->first('PAYPAL_CLIENT_ID') }}</strong>
				</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('PAYPAL_SECRET') ? 'has-error': ''}} ">
                            <label for="inputPassword4">PAYPAL_SECRET</label>
                            <input type="text" name="PAYPAL_SECRET" class="form-control" value="{{old('PAYPAL_SECRET',\Setting::get('PAYPAL_SECRET'))}}">

                            @if($errors->has('PAYPAL_SECRET'))
                                <span class="help-block">
					<strong>{{ $errors->first('PAYPAL_SECRET') }}</strong>
				</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('PAYPAL_IS_LIVE') ? 'has-error': ''}} ">
                            <label>Mode</label>
                            <select class="form-control" name="PAYPAL_IS_LIVE">
                                <option value="1" {{Setting::get('PAYPAL_IS_LIVE') == 1 ? 'selected=selected':''}} >Live</option>
                                <option value="0" {{Setting::get('PAYPAL_IS_LIVE') == 0 ? 'selected=selected':''}}>SandBox</option>
                            </select>
                            @if($errors->has('PAYPAL_IS_LIVE'))
                                <span class="help-block">
					<strong>{{ $errors->first('PAYPAL_IS_LIVE') }}</strong>
				</span>
                            @endif
                        </div>


                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                    <!--ads-->
                </div>
            </div>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
@stop
