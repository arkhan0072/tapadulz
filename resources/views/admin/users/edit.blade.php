@extends('layouts.master')

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Services</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
                <h4 class="page-title">Add New Service</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('users.update',$user->id) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="fullname">Full Name</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',$user->name) }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="emailaddress">Email address</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',$user->email) }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="validationCustom02">Description</label>
                            <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="editor" rows="12" cols="5">{{old('description',$user->description)}}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Country</label>
                            <select name="country" class="form-control @error('country') is-invalid @enderror">
                                <option value="" readonly>Select Country</option>

                                @foreach($countries as $country)
                                    <option value="{{$country->id}}" {{$user->city->country_id == $country->id ? 'selected':''}}>{{$country->name}}</option>
                                @endforeach
                            </select>

                            @error('country')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>City</label>
                            <select name="city" class="form-control @error('city') is-invalid @enderror">
                                <option value="" readonly>Select City</option>

                                @foreach($cities as $city)
                                    <option value="{{$city->id}}" {{$user->city->id == $city->id ? 'selected':''}}>{{$city->name}}</option>
                                @endforeach
                            </select>

                            @error('city')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>User Type</label>
                            <select name="user_level" class="form-control @error('user_level') is-invalid @enderror">
                                <option value="" readonly>Select User Level/Permission</option>
                                <option value="0" {{$user->user_level == 0 ? 'selected':''}}>User</option>
                                <option value="1" {{$user->user_level == 1 ? 'selected':''}}>Admin</option>
                            </select>

                            @error('user_level')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Block User</label>
                            <select name="is_blocked" class="form-control @error('is_blocked') is-invalid @enderror">
                                <option value="" readonly>Select User Level/Permission</option>
                                <option value="0" {{$user->is_blocked == 0 ? 'selected':''}}>Blocked</option>
                                <option value="1" {{$user->is_blocked == 1 ? 'selected':''}}>Access Granted</option>
                            </select>

                            @error('is_blocked')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <img src="{{asset('public/images/users')}}/{{$user->load('picture')->picture->name}}" width="67">
                            <div class="custom-control custom-checkbox">
                                <input type="file" class="form-control" name="picture">
{{--                                <label class="custom-control-label" for="checkbox-signup">I accept <a href="javascript: void(0);" class="text-dark">Terms and Conditions</a></label>--}}
                            </div>
                        </div>
                        <div class="form-group mb-0 text-center">
                            <button class="btn btn-success btn-block" type="submit"> Update User </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection

@section('script')

    <!-- Plugin js-->
    <script src="{{ URL::asset('public/ubold/assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('public/ubold/assets/js/pages/form-validation.init.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection

