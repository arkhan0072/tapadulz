<a href="#" onclick="frmdlt{{$user->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$user->id}}" action="{{ route('users.destroy', $user->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
