<a href="#" onclick="frmdlt{{$service->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$service->id}}" action="{{ route('services.destroy', $service->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
