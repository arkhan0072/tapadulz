@extends('frontend.master')

@section('content')
    <section class="description"><!-- start .description -->
        <div class="container">
            <div class="main mx-auto">
                <div class="description-area mx-auto"></div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="sidebar">
                            @if(\Illuminate\Support\Facades\Auth::user())
                                <div class="item">
                                    <div class="exchange-link mx-auto">
                                        <a class="text-center text-white" href="{{route('services.create')}}">Add New
                                            Service</a>
                                    </div>
                                </div>
                            @endif

                            <div class="item bg-gray">
                                <h5 class="text-center">Do you have any other requirement?</h5>
                                <div class="quote-link mx-auto">
                                    <a class="text-white" href="#">Get a quote</a>
                                </div>
                            </div>

                            <div class="item">
                                <h4>About the seller</h4>

                                <figure class="mx-auto">
                                    <img src="{{asset('public/images/user3.png')}}" alt="" width="193" height="190">
                                </figure>

                                <div class="user-name">
                                    <h5 class="text-center mb-0">{{$user->name}}</h5>
                                    <p class="text-center">{{$user->username}}</p>
                                </div>

                                <div class="rating clearfix">
                                    <div class="clearfix">
                                        <ul class="mx-auto">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>

                                    <p class="text-center">5.0 of 160 reviews</p>
                                </div>

                                <ul>
                                    <li class="clearfix">
                                        <div class="left">
                                            <span>From</span>
                                        </div>

                                        <div class="right">
                                            <span>{{$user->city ? $user->city->country->name:''}}</span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Member Since</span>
                                        </div>

                                        <div class="right">
                                            <span>{{\Carbon\Carbon::parse($user->created_at)->format('Y')}}</span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Avg. Response Time</span>
                                        </div>

                                        <div class="right">
                                            <span>1 Hrs</span>
                                        </div>
                                    </li>
                                </ul>


                                <p>{{$user->description}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                {{--                    <h4 class="header-title">Bootstrap Validation - Normal</h4>--}}
                                {{--                    <p class="sub-header">Provide valuable, actionable feedback to your users with HTML5 form validation–available in all our supported browsers.</p>--}}

                                <form action="{{route('services.store')}}" method="Post"
                                      class="needs-validation novalidate" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Service Name</label>
                                        <input type="text" name="name"
                                               class="form-control @error('name') is-invalid @enderror"
                                               placeholder="Service Name" required>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="validationCustom02">Description</label>
                                        <textarea name="details"
                                                  class="form-control @error('details') is-invalid @enderror"
                                                  id="editor" rows="12" cols="5">{{old('details')}}</textarea>
                                        @error('details')
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Category</label>
                                        <div class="input-group">
                                            <select id="category" name="category"
                                                    class="form-control @error('category') is-invalid @enderror">
                                                <option value="" readonly>Select Category</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>

                                            @error('category')
                                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Sub Category</label>
                                        <div class="input-group">
                                            <select id="subcategory" name="subcategory"
                                                    class="form-control @error('subcategory') is-invalid @enderror">

                                            </select>

                                            @error('subcategory')
                                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Country</label>
                                        <select name="country" id="country"
                                                class="form-control @error('country') is-invalid @enderror">
                                            <option value="" readonly>Select Country</option>

                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>

                                        @error('country')
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>City</label>
                                        <select name="city" id="city" class="form-control @error('city') is-invalid @enderror">
                                        </select>

                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-3">
                                        <label>Duration</label>
                                        <select name="duration" id="duration"
                                                class="form-control @error('duration') is-invalid @enderror">
                                            <option value="" readonly>Days</option>
                                            @for($i=1;$i<=30;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>

                                        @error('duration')
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Upload Pictures</label>
                                        <input type="file" multiple name="pic[]"
                                               class="form-control @error('pic') is-invalid @enderror"
                                               placeholder="Picture" required>
                                        @error('pic')
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Language</label>
                                        <div class="input-group">
                                            <select id="validationCustomUsername03" name="language"
                                                    class="form-control @error('language') is-invalid @enderror">
                                                <option value="" readonly>Select Language</option>
                                                <option value="English">English</option>
                                                <option value="Arabic">Arabic</option>
                                            </select>

                                            @error('language')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                             </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox form-check">
                                            <input type="checkbox" class="custom-control-input" id="invalidCheck"
                                                   required>
                                            <label class="custom-control-label" for="invalidCheck">Agree to terms and
                                                conditions</label>
                                            <div class="invalid-feedback">
                                                You must agree before submitting.
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary" type="submit">Submit form</button>
                                </form>

                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->


                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')

    <!-- Plugin js-->
    <script src="{{ URL::asset('public/ubold/assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('public/ubold/assets/js/pages/form-validation.init.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>


@endsection
