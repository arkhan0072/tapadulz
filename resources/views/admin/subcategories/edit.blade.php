@extends('layouts.master')

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Services</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
                <h4 class="page-title">Add New Service</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    {{--                    <h4 class="header-title">Bootstrap Validation - Normal</h4>--}}
                    {{--                    <p class="sub-header">Provide valuable, actionable feedback to your users with HTML5 form validation–available in all our supported browsers.</p>--}}

                    <form action="{{route('subcategories.update',$subCategory->id)}}" method="Post" class="needs-validation" novalidate>
                        @method('PUT')
                        @csrf
                        <div class="form-group mb-3">
                            <label for="validationCustom01">SubCategory Name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name',$subCategory->name)}}" placeholder="Service Name" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label>Category</label>
                            <select name="category" class="form-control @error('category') is-invalid @enderror">
                                <option value="" readonly>Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{$subCategory->category_id == $category->id ? 'selected':''}}>{{$category->name}}</option>
                                    @endforeach
                            </select>

                            @error('category')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button class="btn btn-primary" type="submit">Submit form</button>
                    </form>

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->


    </div>
    <!-- end row -->

@endsection

@section('script')

    <!-- Plugin js-->
    <script src="{{ URL::asset('public/ubold/assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('public/ubold/assets/js/pages/form-validation.init.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection
