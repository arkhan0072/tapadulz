@extends('layouts.master')

@section('css')
    <!-- third party css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
    <style>
        th.sorting {
            width: 100%!important;
        }
    </style>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                        <li class="breadcrumb-item active">Datatables</li>
                    </ol>
                </div>
                <h4 class="page-title">Datatables</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Services</h4>
                    <p class="text-muted font-13 mb-4">
                    </p>

                    <table id="subcategory-datatable" class="table table-responsive table-bordered" style="width: 100%!important;">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>SubCategory Name</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('script')

    <!-- third party js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->

    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
    <script>
       $(document).ready(function () {


            $('#subcategory-datatable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('Subcategoryajax') }}",
                buttons: [
                    {{--{--}}
                        {{--    text: 'Add Hotel',--}}
                        {{--    action: '{{route('hotels.create')}}',--}}
                        {{--    className:'btn btn-primary'--}}
                        {{--    },--}}
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id',title:'id'},
                    {data: 'name', name: 'name',title:'Name'},
                    {data: 'category.name', name: 'category.name',title:'Name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
