<a href="#" onclick="frmdlt{{$subCategory->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$subCategory->id}}" action="{{ route('subcategories.destroy', $subCategory->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
