@extends('layouts.master')

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Extras</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
                <h4 class="page-title">Profile</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
<div class="col-md-12">
    <h4 class="header-title mb-3">Inbox</h4>

    <div class="inbox-widget slimscroll" style="max-height: 310px;">
        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-2.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Tomaslau</p>
            <p class="inbox-item-text">I've finished it! See you so...</p>
            <p class="inbox-item-date">
               <form  action="{{route('chatCreate')}}" method="Post" enctype="multipart/form-data">
                @csrf
                <input type="text" name="message"/>
                <input type="file" name="file[]" multiple/>
                <input type="text" name="sender_id" value="{{$offer->sender_id}}"/>
                <button type="submit">Send<i class="fa fa-arrow-right"></i> </button>
            </form>
            </p>
        </div>
        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-3.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Stillnotdavid</p>
            <p class="inbox-item-text">This theme is awesome!</p>
            <p class="inbox-item-date">
                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
            </p>
        </div>
        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-4.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Kurafire</p>
            <p class="inbox-item-text">Nice to meet you</p>
            <p class="inbox-item-date">
                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
            </p>
        </div>

        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-5.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Shahedk</p>
            <p class="inbox-item-text">Hey! there I'm available...</p>
            <p class="inbox-item-date">
                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
            </p>
        </div>
        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-6.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Adhamdannaway</p>
            <p class="inbox-item-text">This theme is awesome!</p>
            <p class="inbox-item-date">
                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
            </p>
        </div>

        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-3.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Stillnotdavid</p>
            <p class="inbox-item-text">This theme is awesome!</p>
            <p class="inbox-item-date">
                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
            </p>
        </div>
        <div class="inbox-item">
            <div class="inbox-item-img"><img src="assets/images/users/user-4.jpg" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">Kurafire</p>
            <p class="inbox-item-text">Nice to meet you</p>
            <p class="inbox-item-date">
                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
            </p>
        </div>
    </div> <!-- end inbox-widget -->

</div> <!-- end card-box-->

</div> <!-- end col-->
@endsection
