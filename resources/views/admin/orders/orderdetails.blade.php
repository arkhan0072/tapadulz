@extends('frontend.master')
@section('content')
<Style>
    a.text-white.cancel.btn.btn-danger{
        line-height: 40px;
    }
    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
    }
</Style>
<section class="order"><!-- start .order -->
    <div class="container">
        <div class="order-area mx-auto">
    <div class="row">
        <div class="col-md-12 col-lg-8">
            <div class="order-item">
                <div class="order-desc">
                    <div class="clearfix">
                        <div class="title">
                            <h4>{{$order->provided->service->name}}</h4>
                        </div>

                        <div class="status">
                            @if($order->provided->status==2)
                                <p>Under Process</p>
                            @elseif($order->provided->status==3)
                                <p>Delivered</p>
                            @elseif($order->provided->status==4)
                                <p>Accepted</p>
                            @elseif($order->provided->status==5)
                                <p>Completed</p>
                            @endif
                        </div>
                    </div>

                    <div class="border"></div>
                    <p>sadfsadf asd</p>
                    <div class="clearfix">
                        <div class="action clearfix">
                        </div>
                    </div>
                </div>
            </div>
            @foreach($order->needed->deliveries as $delivery)
                @if($delivery->status==2)
                    <style>
                        .border-color-reject{
                            border: 1px solid red!important;
                        }
                    </style>
                    <div class="order-item border-color-reject">
                        @elseif($delivery->status==1)
                            <style>
                                .border-color-accept{
                                    border: 1px solid lime!important;
                                }
                            </style>
                            <div class="order-item border-color-accept">
                                @else
                                    <div class="order-item">
                                        @endif

                                        <div class="order-desc">
                                            <div class="clearfix">
                                                <div class="title">
                                                    <h4>{{$delivery->message}}</h4>
                                                </div>
                                                <div class="status">
                                                    @if($delivery->status=='')
                                                        <p>Delivered</p>
                                                    @elseif($delivery->status==1)
                                                        <p>Accepted</p>
                                                    @elseif($delivery->status==2)
                                                        <p>Rejected</p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="border"></div>
                                            <ul>
                                                @foreach($delivery->files as $file)
                                                    <li><a href="{{route('download',$file->name)}}">{{$file->name}}</a> </li>
                                                @endforeach
                                            </ul>
                                            <div class="clearfix">
                                                @if($delivery->status=='')
                                                    <div class="action clearfix">
                                                        <div class="accept ml-4">
                                                            <a class="Accpet text-white" href="#" data-service_id="{{$order->needed->service->id}}" data-offer_id="{{$order->id}}" data-delivery_id="{{$delivery->id}}">Accept</a>
                                                        </div>

                                                        <div class="reject ml-4">
                                                            <a href="#" class="Reject" data-service_id="{{$order->needed->service->id}}" data-offer_id="{{$order->id}}" data-delivery_id="{{$delivery->id}}">Reject</a>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @if(sizeof($order->needed->service->ratings)>0 && $order->id==Request::segment(4) && $order->provided->service->user->id==\Illuminate\Support\Facades\Auth::id())
                                        <div class="reviews-box">
                                            <div class="top clearfix">
                                                <div class="top-info mb-0 clearfix">
                                                    <div class="name px-0">
                                                        <h5>Give A Review</h5>
                                                    </div>

                                                    <div class="star mb-3">
                                                        <ul>
                                                            <li class="d-flex">
                                                                <span class="pr-3">write something about</span>
                                                                <select class="stars stars-example-fontawesome example-fontawesome" name="q1" autocomplete="off">
                                                                    <option value="1" {{$order->needed->service->ratings[0]->q1 == 1 ? 'selected':''}}>1</option>
                                                                    <option value="2" {{$order->needed->service->ratings[0]->q1 == 2 ? 'selected':''}}>2</option>
                                                                    <option value="3" {{$order->needed->service->ratings[0]->q1 == 3 ? 'selected':''}}>3</option>
                                                                    <option value="4" {{$order->needed->service->ratings[0]->q1 == 4 ? 'selected':''}}>4</option>
                                                                    <option value="5" {{$order->needed->service->ratings[0]->q1 == 5 ? 'selected':''}}>5</option>
                                                                </select>
                                                            </li>
                                                            <li class="d-flex">
                                                                <span class="pr-3">write something about</span>
                                                                <select class="stars stars-example-fontawesome example-fontawesome" name="q2" autocomplete="off">
                                                                    <option value="1" {{$order->needed->service->ratings[0]->q2 == 1 ? 'selected':''}}>1</option>
                                                                    <option value="2" {{$order->needed->service->ratings[0]->q2 == 2 ? 'selected':''}}>2</option>
                                                                    <option value="3" {{$order->needed->service->ratings[0]->q2 == 3 ? 'selected':''}}>3</option>
                                                                    <option value="4" {{$order->needed->service->ratings[0]->q2 == 4 ? 'selected':''}}>4</option>
                                                                    <option value="5" {{$order->needed->service->ratings[0]->q2 == 5 ? 'selected':''}}>5</option>
                                                                </select>
                                                            </li>

                                                            <li class="d-flex">
                                                                <span class="pr-3">write something about</span>
                                                                <select class="stars stars-example-fontawesome example-fontawesome" name="q3" autocomplete="off">
                                                                    <option value="1" {{$order->needed->service->ratings[0]->q3 == 1 ? 'selected':''}}>1</option>
                                                                    <option value="2" {{$order->needed->service->ratings[0]->q3 == 2 ? 'selected':''}}>2</option>
                                                                    <option value="3" {{$order->needed->service->ratings[0]->q3 == 3 ? 'selected':''}}>3</option>
                                                                    <option value="4" {{$order->needed->service->ratings[0]->q3 == 4 ? 'selected':''}}>4</option>
                                                                    <option value="5" {{$order->needed->service->ratings[0]->q3 == 5 ? 'selected':''}}>5</option>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="timeline mb-3">
                                                <p>{{\Carbon\Carbon::parse($order->needed->service->ratings[0]->created_at)->diffForHumans()}}</p>
                                            </div>

                                            <div class="form-group">
                                                <textarea class="form-control" name="review_text" disabled>{{$order->needed->service->ratings[0]->review_text}}</textarea>
                                            </div>
                                        </div>
                        @section('scripts')
                            <script>
                                $(function() {
                                    $('.example-fontawesome').barrating({
                                        theme: 'fontawesome-stars',
                                        readonly: true
                                    });
                                });
                            </script>
                        @endsection
                        @elseif($order->needed->status==4 && $order->provided->status==4)
                            <form action="{{route('ratings.store')}}" method="post">
                                @csrf
                                <div class="reviews-box">
                                    <div class="top clearfix">
                                        <div class="top-info mb-0 clearfix">
                                            <div class="name px-0">
                                                <h5>Give A Review</h5>
                                            </div>

                                            <div class="star mb-3">
                                                <ul>
                                                    <li class="d-flex">
                                                        <span class="pr-3">write something about</span>
                                                        <select class="stars stars-example-fontawesome example-fontawesome" name="q1" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </li>
                                                    <li class="d-flex">
                                                        <span class="pr-3">write something about</span>
                                                        <select class="stars stars-example-fontawesome example-fontawesome" name="q2" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </li>

                                                    <li class="d-flex">
                                                        <span class="pr-3">write something about</span>
                                                        <select class="stars stars-example-fontawesome example-fontawesome" name="q3" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="review_text"></textarea>
                                    </div>
                                    <input type="hidden" name="service_id" value="{{$order->needed->service->id}}">
                                    <button class="btn text-white" type="submit">Submit</button>
                                </div>
                            </form>
                        @section('scripts')
                            <script>
                                $(function() {
                                    $('.example-fontawesome').barrating({
                                        theme: 'fontawesome-stars'
                                    });
                                });
                            </script>
                        @endsection
                        @endif
                            </div>
                            <div class="col-md-12 col-lg-4 sidebar">
                                <div class="item">
                                    <div class="row mx-0">
                                        <div class="col-7 px-0">
                                            <h4 class="mb-2">Order Info</h4>
                                        </div>
                                    </div>

                                    <div class="contact-form">
                                        <form class="bg-white">
                                            <div class="form-group clearfix">
                                                <label class="text-left">Order Id</label>
                                                <input class="bg-gray"  value="{{"X-1081-0".$order->id}}" type="text" readonly>
                                            </div>
                                            <div class="form-group clearfix">

                                                <label class="text-left">Status {{$order->provided->service->user->username}}</label>

                                                @if($order->provided->status==2)
                                                    <input class="bg-gray" type="text" value="Under Process" readonly>
                                                @elseif($order->provided->status==3)
                                                    <input class="bg-gray" type="text" value="Delivered" readonly>
                                                @elseif($order->provided->status==4)
                                                    <input class="bg-gray" type="text" value="Accepted" readonly>
                                                @elseif($order->provided->status==5)
                                                    <input class="bg-gray" type="text" value="Completed" readonly>
                                                @endif
                                            </div>

                                            <div class="form-group clearfix">
                                                <label class="text-left">Status {{$order->needed->service->user->username}}</label>
                                                @if($order->needed->status==2)
                                                    <input class="bg-gray" type="text" value="Under Process" readonly>
                                                @elseif($order->needed->status==3)
                                                    <input class="bg-gray" type="text" value="Delivered" readonly>
                                                @elseif($order->needed->status==4)
                                                    <input class="bg-gray" type="text" value="Accepted" readonly>
                                                @elseif($order->needed->status==5)
                                                    <input class="bg-gray" value="Completed" type="text" readonly>
                                                @endif
                                            </div>

                                            <div class="form-group clearfix">
                                                <label class="text-left">Over All Status</label>
                                                <input class="bg-gray" value="{{$order->status == 5 ? 'Completed':'Under Process'}}" type="text" readonly>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="exchange-link mx-auto">
                                        <a class="text-center text-white px-5" href="{{route('messages')}}">Chat</a>
                                    </div>
                                    <div class="exchange-link mx-auto mt-2">
                                        <a class="text-center text-white px-5" href="{{route('history',$order->id)}}">Details</a>
                                    </div>
                                </div>
                            </div>
                    </div>

        </div>

    </div>
    @endsection
    @section('scripts')
<script>
    $(document).ready(function () {
            $('.Accpet').click(function (e) {
                e.preventDefault();
                var service_id=$(this).data('service_id');
                var offer_id=$(this).data('offer_id');
                var delivery_id=$(this).data('delivery_id');
                $.ajax({
                    url:"{{route('accepted')}}",
                    type:'get',
                    data:{
                        offer_id:offer_id,
                        service_id:service_id,
                        delivery_id:delivery_id
                    },
                    success:function (data) {
                        console.log(data);
                        location.reload();
                    }
                })
            });
        $('.Reject').click(function (e) {
            e.preventDefault();
            var service_id=$(this).data('service_id');
            var offer_id=$(this).data('offer_id');
            var delivery_id=$(this).data('delivery_id');
            $.ajax({
                url:"{{route('DeliveryReject')}}",
                type:'post',
                data:{
                    '_token':"{{csrf_token()}}",
                    offer_id:offer_id,
                    service_id:service_id,
                    delivery_id:delivery_id
                },
                success:function (data) {
                    console.log(data);
                    location.reload();
                }
            })
        })
    })
</script>



@endsection
