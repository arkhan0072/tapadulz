@extends('layouts.master')

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Services</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
                <h4 class="page-title">Add New Service</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <div class="col-md-6 offset-3">
                        <div class="x_panel">
                            <div class="x_title">
<form class="form-horizontal" method="POST" id="payment-form" role="form" action="{!! URL::route('checkout.payment') !!}" >
    {{ csrf_field() }}

    {{--<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">--}}
    {{--<div class="col-md-6">--}}
    {{--<input id="amount" type="hidden" class="form-control" name="amount" value="{{\Auth::User()->cart->products()->sum('price')}}" autofocus >--}}
    {{--@if ($errors->has('amount'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('amount') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}


    <div class="form-group">
        <div style="margin-left: 10px;">
            <button type="submit" class="stripe-button-el">
                <span style="display: block; min-height: 30px;">Pay with Paypal</span>
            </button>
        </div>
    </div>
</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
