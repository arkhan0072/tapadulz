jQuery(document).ready(function () {
    if (jQuery('.seller-carousel')[0]) {
        jQuery(".seller-carousel").owlCarousel({
            items: 4,
            loop: false,
            nav: true,
            dots: false,
            smartSpeed: 900,
            autoplay: true,
            autoplayTimeout: 5000,
            fallbackEasing: 'easing',
            transitionStyle: "fade",
            autoplayHoverPause: true,
            animateOut: 'fadeOut',
            responsive: {
                0: {
                    items: 1
                },
                750: {
                    items: 2
                },
                1035: {
                    items: 3
                },
                1348: {
                    items: 4
                }
            }
        });
    }
});
jQuery(document).ready(function () {
    if (jQuery('.seller2-carousel')[0]) {
        jQuery(".seller2-carousel").owlCarousel({
            items: 4,
            loop: false,
            nav: true,
            dots: false,
            smartSpeed: 900,
            autoplay: true,
            autoplayTimeout: 5000,
            fallbackEasing: 'easing',
            transitionStyle: "fade",
            autoplayHoverPause: true,
            animateOut: 'fadeOut',
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 3
                },
                1059: {
                    items: 4
                },
                1200: {
                    items: 5
                }
            }
        });
    }
});

jQuery('#home-slider').flexslider({
    animation: "fade",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#video-slider').flexslider({
    animation: "slide",
    controlNav: false,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller1-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller2-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller3-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller4-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller5-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller6-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller7-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller8-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller9-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller10-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller11-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller12-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller13-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller14-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller15-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller16-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery('#seller17-slider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 2000,
    start: function(slider){
        jQuery('body').removeClass('loading');
    }
});

jQuery(document).ready(function () {
    jQuery("#myTab a").click(function(e){
        e.preventDefault();
        jQuery(this).tab('show');
    });
});

jQuery(document).ready(function() {
    // var nice = $("html").niceScroll();
    jQuery("#boxscroll").niceScroll({
        cursorborder:"",
        cursorwidth: 12,
        cursorcolor:"#1299b9",
        boxzoom:true
    });
});

