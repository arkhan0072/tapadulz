<?php

namespace App;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    protected $fillable = ['name','slug','details','subcategory_id','duration','city_id','created_by'];

    public function user(){
        return $this->belongsTo('App\User','created_by');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function subcategory(){
        return $this->belongsTo('App\SubCategory');
    }
    public function city(){
        return $this->belongsTo('App\City');
    }
    public function pictures(){
        return $this->hasMany('App\Picture');
    }
    public function order(){
        return $this->hasMany('App\Order');
    }
    public function offer(){
        return $this->hasMany('App\Offer');
    }
    public function ratings(){
        return $this->hasMany(Rating::Class);
    }

}
