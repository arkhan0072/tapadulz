<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OfferItem extends Model
{
    public function offer(){
        return $this->belongsTo('App\Offer');
    }

    public function service(){
        return $this->belongsTo(Service::class)->with('user','ratings');
    }

    public function deliveries(){
        return $this->hasMany(Delivery::class)->with('files');
    }


}
