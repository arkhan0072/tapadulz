<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItemFile extends Model
{
   public function delivery()
   {
       return $this->hasMany(Delivery::class);
   }
}
