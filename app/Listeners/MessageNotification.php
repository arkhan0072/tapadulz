<?php

namespace App\Listeners;

use App\Events\messenger_event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MessageNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  messenger_event  $event
     * @return void
     */
    public function handle(messenger_event $event)
    {
        //
    }
}
