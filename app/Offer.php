<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Offer extends Model
{
    protected $appends = ['needed', 'provided'];

    public const STATUS_OFFERED = 1;
    public const STATUS_ACCEPTED= 2;
    public const STATUS_COMPLETED = 3;


   public function user(){
       return $this->belongsToMany('App\User');
   }
    public function items(){
        return $this->hasMany('App\OfferItem')->with('service','deliveries');
    }
    public function getNeededAttribute(){
        return $this->items()->whereHas('service', function ($q){
            $q->where('created_by','<>', Auth::id());
        })->first();
    }
    public function getProvidedAttribute(){
        return $this->items()->whereHas('service', function ($q){
            $q->where('created_by','=', Auth::id());
        })->first();
    }



}
