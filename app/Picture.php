<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{

    public function service(){
        return $this->belongsTo('App\Service');
    }
    public function user(){
        return $this->belongsToMany('App\User');
    }
}
