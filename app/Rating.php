<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function service(){
        return $this->belongsTo(Service::Class,'service_id');
    }
}
