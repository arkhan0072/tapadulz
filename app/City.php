<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   public function country(){
       return $this->belongsTo('App\Country','country_id');
   }
   public function user(){
       return $this->belongsTo('App\User');
   }
}
