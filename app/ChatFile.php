<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatFile extends Model
{
   public function message()
   {
       return $this->belongsTo('App\Messenger');
   }
}
