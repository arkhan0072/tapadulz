<?php

namespace App\Http\Controllers;

use App\Category;
use App\Search;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();
        $max = Service::max('amount');
        $maxDuration = Service::max('duration');
        return view('search', compact('categories', 'max', 'maxDuration'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ajaxSearch(Request $request)
    {
//return $request;
        $result = Service::with('category', 'user.picture','pictures')
            ->when($request->key, function ($q) use ($request) {
                $q->orWhere('name', 'like', '%' . $request->key . '%')
                    ->orWhere('details', 'like', '%' . $request->key . '%')
                    ->orWhere('slug', 'like', '%' . $request->key . '%');
            })
            ->when($request->status, function ($q) use ($request) {
                $q->whereHas('user', function ($i) use ($request) {
                    $i->where('status', $request->status);

                });
            })
            ->when($request->amount, function ($q) use ($request) {
                $q->where('amount', 'like', '%' . $request->amount);
            })
            ->when($request->category, function ($q) use ($request) {
                $q->where('category_id', $request->category);
            })
            ->when($request->language, function ($q) use ($request) {
                $q->where('language', $request->language);
            })
            ->when($request->duration, function ($q) use ($request) {
                $q->where('duration', $request->duration);
            })


            ->get()->sortBy('name');

        $html = view('search_partial', compact('result'))->render();
        return $html;
    }
//        elseif($request->type1=='user'){
//            $type=$request->type1;
//            $result = User::where('name', $request->key)->orWhere('username', $request->key)->with('picture')->take($request->limit)->get();
//            $html= view('search_partial',compact('result','type'))->render();
//            return $html;
//        }elseif($request->type1=='service'){
//            $type=$request->type1;
//            $result = Service::all();
//            $html= view('search_partial',compact('result','type'))->render();
//            return $html;
//        }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Search $search
     * @return Response
     */
    public function show(Search $search)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Search $search
     * @return Response
     */
    public function edit(Search $search)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Search $search
     * @return Response
     */
    public function update(Request $request, Search $search)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Search $search
     * @return Response
     */
    public function destroy(Search $search)
    {
        //
    }
}
