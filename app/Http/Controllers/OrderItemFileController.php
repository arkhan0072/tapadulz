<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Delivery;
use App\Offer;
use App\OfferItem;
use App\OrderItemFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class OrderItemFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request;
        $delivers = new Delivery();
        $delivers->message=$request->message;
        $delivers->user_id=Auth::id();
        $delivers->offer_item_id=$request->provided_id;
        $delivers->save();
        if($request->has('delivery')){
            foreach ($request->delivery as $delivery){
                $attachment= new Attachment();
                $attachment->delivery_id= $delivers->id;
                $file_extension = $delivery->getClientOriginalExtension();
                $file_path = $delivery->getFilename();
                $filename = 'tapadul_' . Str::random(8) . '.' . $file_extension;
                $attachment->name = $filename;
                Storage::disk('OrderFile')->put($filename, \File::get($delivery));
                $attachment->save();
            }
        }

        $orderItems=OfferItem::where('id',$request->provided_id)->first();
        $orderItems->status=3;
        $orderItems->save();
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderItemFile  $orderItemFile
     * @return \Illuminate\Http\Response
     */
    public function show(OrderItemFile $orderItemFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderItemFile  $orderItemFile
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderItemFile $orderItemFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderItemFile  $orderItemFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderItemFile $orderItemFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderItemFile  $orderItemFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderItemFile $orderItemFile)
    {
        //
    }
}
