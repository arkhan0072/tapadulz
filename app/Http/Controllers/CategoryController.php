<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Categoryajax()
    {
        $category = Category::where('id', '>', 0);

//        return $users;
        return Datatables::of($category)
            ->addColumn('action', function (Category $category) {
                $btn = '<a href="' . route("categories.show", $category->id) . '"><i class="fa fa-eye"></i></a>
	   	<a href="' . route("categories.edit", $category->id) . '"><i class="fa fa-edit"></i></a>';
                return $btn . view('admin.categories.delete', compact('category'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function create()
    {
       return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'string', 'max:255'],
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        return redirect(route('categories.index'))->with('msg', 'Category Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'string', 'max:255'],
        ]);
        $category->name = $request->name;
        $category->save();
        return redirect(route('categories.index'))->with('msg', 'Category Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('categories.index'))->with('msg', 'Category Deleted Successfully');
    }
}
