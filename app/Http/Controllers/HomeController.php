<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gprofile(Request $request)
    {
        $user = User::where('id',$request->id)->first();
        $user->load('city');
        return view('admin.users.profile', compact('user'));
    }
    public function index()
    {
        return "ok";
        if(Auth::user()->user_level==1){
        return redirect(route('ahome'));
        }else{
            return redirect(route('uhome'));
        }
    }

}
