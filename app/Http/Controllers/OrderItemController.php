<?php

namespace App\Http\Controllers;

use App\OfferItem;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OfferItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function show(OfferItem $orderItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OfferItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OfferItem $orderItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OfferItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OfferItem $orderItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OfferItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfferItem $orderItem)
    {
        //
    }
}
