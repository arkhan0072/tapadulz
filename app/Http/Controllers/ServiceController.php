<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Country;
use App\Picture;
use App\Service;
use App\SubCategory;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        return  view('admin.services.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        $categories=Category::all();
        $subcategories=SubCategory::all();
        $countries=Country::all();
        $cities=City::all();
        return view('admin.services.create',compact('countries','cities','subcategories','user','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'name' => 'required|max:255|min:5',
            'details' => 'required|min:150',
            'subcategory' => 'required',
            'category' => 'required',
            'duration' => 'required|min:1',
            'city' => 'required',
            'language' => 'required',
            'pic' => 'required',
        ]);
//        return $request->details;
        $user=Auth::user();
        $service=new Service();
        $service->name=$request->name;
        $slug = SlugService::createSlug(Service::class, 'slug', $request->name);
//        $service->slug = $slug;
        $service->details=strip_tags($request->details);
        $service->category_id=$request->category;
        $service->subcategory_id=$request->subcategory;
        $service->duration=$request->duration;
        $service->city_id=$request->city;
        $service->created_by=Auth::id();
        $service->language=$request->language;
        $service->save();

        if($request->has('pic')){
            foreach($request->pic as $picture){
                $pictures = new Picture();
                $pictures->service_id = $service->id;
                $file_extension = $picture->getClientOriginalExtension();
                $file_path = $picture->getFilename();
                $filename = $service->id . '_' . Str::random(8) . '.' . $file_extension;
                $pictures->name = $filename;
                Storage::disk('services')->put($filename, \File::get($picture));
                $pictures->save();
            }
        }
        return  redirect(route('services.index'))->with(['msg','Service Created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function Serviceajax(){
        if(Auth::user()->user_level==1){
             $service=Service::where('id','>',0)->with('subcategory.category','city.country');
             $services=Service::where('created_by',Auth::id())->with('subcategory.category','city.country')->get();
        }else{
            $service=Service::where('created_by',Auth::id())->with('subcategory.category','city.country');
            $services=Service::where('created_by',Auth::id())->with('subcategory.category','city.country')->get();
        }
        return Datatables::of($service)
            ->addColumn('action', function(Service $service) use ($services){
                $btn = '<a href="' . route("services.show", $service->id) . '"><i class="fa fa-eye"></i></a>
	   	<a href="'.route("services.edit", $service->id).'"><i class="fa fa-edit"></i></a>';
                if(Auth::user()->id!=$service->created_by){
            $btn.=view('admin.services.offer', compact('service','services'))->render();
                }
                return $btn.view('admin.services.delete', compact('service'))->render();
            })
            ->addColumn('details',function (Service $service){
                return $details=str_replace('&nbsp;',' ',$service->details);
            })
            ->addColumn('Location',function (Service $service){
                return $details=$service->city->name.','.$service->city->country->name;
            })
            ->addColumn('Category',function (Service $service){
                return $details=$service->subcategory->category->name.','.$service->subcategory->name;
            })
            ->rawColumns(['Category','Location','details','action'])
            ->make(true);
    }

    public function show(Service $service)
    {
        return $service;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        if(Auth::id()==$service->created_by || Auth::user()->user_level==1){
            $user=Auth::user();
            $categories=Category::all();
        $subcategories=SubCategory::all();
        $countries=Country::all();
        $cities=City::all();
        $service->load('subcategory.category','city.country');

       return view('admin.services.edit',compact('subcategories','countries','cities','service','user','categories'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
//        return $service;
        $request->validate([
            'name' => 'required|max:255|min:5',
            'details' => 'required|min:150',
            'subcategory' => 'required',
            'category' => 'required',
            'duration' => 'required|min:1',
            'city' => 'required',
            'language' => 'required',
            'pic' => 'required',
        ]);
        $service->name=$request->name;
        $service->details=strip_tags($request->details);
        $service->category_id=$request->category;
        $service->subcategory_id=$request->subcategory;
        $service->duration=$request->duration;
        $service->city_id=$request->city;
        $service->language=$request->language;
        $service->save();
        if($request->has('pic')){
            foreach($request->pic as $picture){
                $pictures = new Picture();
                $pictures->service_id = $service->id;
                $file_extension = $picture->getClientOriginalExtension();
                $file_path = $picture->getFilename();
                $filename = $service->id . '_' . Str::random(8) . '.' . $file_extension;
                $pictures->name = $filename;
                Storage::disk('services')->put($filename, \File::get($picture));
                $pictures->save();
            }
        }
        return redirect(route('services.index'))->with(['msg','Service updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect(route('services.index'))->with(['msg','Service Deleted Successfully']);

    }
}
