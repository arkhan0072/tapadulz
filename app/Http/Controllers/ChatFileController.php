<?php

namespace App\Http\Controllers;

use App\ChatFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ChatFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    public function file($name)
    {
        $chat=ChatFile::where('name',$name)->first();
        return "<img src='public/images/$chat->name' >";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('files')){
            $url=[];
            foreach($request->file('files')  as $key=>$chatFile){

                $chat = new ChatFile();
                $chat->conversation_id = $request->conversation_id;
                $file_extension = $chatFile->getClientOriginalExtension();
                $file_path = $chatFile->getFilename();
                $filename = 'tapadul_' . Str::random(8) . '.' . $file_extension;
                $chat->name = $filename;
                Storage::disk('chatFile')->put($filename, \File::get($chatFile));
                $chat->url='<a href="http://tapadulz.ar/download/'.$filename.'">'.$chat->name.'</a><br>';
//                $chat->url='http://tapadulz.ar/download/'.$filename;
                $chat->save();
                array_push($url,$chat->url);
            }

            foreach($url as $key=>$ul){
               $data[]=$ul;
            }

        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function show(ChatFile $chatFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatFile $chatFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatFile $chatFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatFile $chatFile)
    {
        //
    }
}
