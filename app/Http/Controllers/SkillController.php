<?php

namespace App\Http\Controllers;

use App\Skill;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.skills.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.skills.create');

    }

    public function Skillajax()
    {
        $skill = Skill::where('id', '>', 0);
//        return response()->json($service);
        return Datatables::of($skill)
            ->addColumn('action', function (Skill $skill) {
                $btn = '<a href="' . route("skills.show", $skill->id) . '"><i class="fa fa-eye"></i></a>
	   	<a href="' . route("skills.edit", $skill->id) . '"><i class="fa fa-edit"></i></a>';

                return $btn . view('admin.skills.delete', compact('skill'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public
    function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:50', 'min:5']
        ]);
        $skill = new Skill();
        $skill->name = $request->name;
        $slug = SlugService::createSlug(Skill::class, 'slug', $request->name);
        $skill->save();
        return redirect(route('skills.index'))->with(['msg', 'Skill Created Successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param Skill $skill
     * @return Response
     */
    public
    function show(Skill $skill)
    {
      return $skill;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Skill $skill
     * @return Response
     */
    public
    function edit(Skill $skill)
    {
        return view('admin.skills.edit', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Skill $skill
     * @return Response
     */
    public
    function update(Request $request, Skill $skill)
    {
        $request->validate([
            'name' => ['required', 'max:50', 'min:5']
        ]);
        $skill->name = $request->name;
        $slug = SlugService::createSlug(Skill::class, 'slug', $request->name);
        $skill->save();
        return redirect(route('skills.index'))->with(['msg', 'Skill Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Skill $skill
     * @return Response
     */
    public
    function destroy(Skill $skill)
    {
        $skill->delete();
        return redirect(route('skills.index'))->with(['msg', 'Skill Deleted Successfully']);

    }
}
