<?php

namespace App\Http\Controllers;

use App\Chat;
use App\ChatFile;
use App\Delivery;
use App\Offer;
use App\OfferItem;
use App\Order;
use BaklySystems\LaravelMessenger\Facades\Messenger;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Pusher\Pusher;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('admin.offers.recieved');
    }

    public function ajaxOffers(Request $request)
    {
        $orders = Offer::where('status', '<>', Offer::STATUS_OFFERED)->get();
//        return $orders;
        $html = view('admin.offers.offers_partial', compact('orders'))->render();
        return $html;


    }


    public function offerz(Request $request)
    {
        $offers = Offer::where('status', Offer::STATUS_OFFERED)->get();
//        return $offers;
        $html = view('admin.offers.offers_results', compact('offers'))->render();
        return $html;


    }

    public function offer()
    {
        return view('admin.offers.offers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        return $request;
        $offer = Offer::where('receiver_id', Auth::id())->first();
        if (Auth::id() == $request->reciever_id) {
            $request->session()->flash('message', 'You can not send offer to your self.');
            $request->session()->flash('message-type', 'danger');
            return response()->json(['status' => 'error']);
        }else {
            $offer = new Offer();
            $offer->sender_id = Auth::id();
            $offer->receiver_id = $request->reciever_id;
            $offer->status = 1;
            $offer->save();
            for ($i = 1; $i <= 2; $i++) {
                if ($i == 1) {
                    $service_id = $request->service_needed;
                } else {
                    $service_id = $request->service_provided;
                }
                $offerItems = new OfferItem();
                $offerItems->offer_id = $offer->id;
                $offerItems->service_id = $service_id;
                $offerItems->status = 1;
                $offerItems->save();
            }
            $offer->user()->sync($offer->id);
            $request->session()->flash('message', 'Offer Send successfully.');
            $request->session()->flash('message-type', 'success');
            return response()->json(['status' => 'success']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Offer $offer
     * @return Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Offer $offer
     * @return Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Offer $offer
     * @return Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Offer $offer
     * @return Response
     */
    public function destroy(Offer $offer)
    {
        //
    }

    public function accpet(Request $request)
    {
        $message = '';
        $offer = Offer::where('id', $request->offer_id)->first();
        $offer->status = 2;
        $offer->save();
        $order = new Order();
        $order->offer_id = $offer->id;
        $order->save();
        for ($i = 1; $i <= 2; $i++) {
            $offerItems = OfferItem::where('offer_id', $offer->id)->get();
            foreach ($offerItems as $item) {
                $item = OfferItem::where('id', $item->id)->first();
                $item->status = 2;
                $item->save();
            }
        }


        $authId = auth()->id();
        $withId = $request->withId;
        $conversation=Messenger::getConversation($offer->sender_id, $offer->receiver_id);
        if(!$conversation){
            $conversation = Messenger::newConversation($offer->sender_id, $offer->receiver_id);
            $message .= Messenger::newMessage($conversation->id, $offer->receiver_id, 'Hello!');
        }

        if ($request->has('attachments')) {
            foreach ($request->attachments as $attachment) {
                $chatFile = new ChatFile();
                $chatFile->message_id = $message->id;
                $chatFile->conversation_id = $conversation->id;
                $file_extension = $attachment->getClientOriginalExtension();
                $file_path = $attachment->getFilename();
                $filename = $message->id . '_' . Str::random(8) . '.' . $file_extension;
                $chatFile->name = $filename;
                Storage::disk('chatFile')->put($filename, File::get($attachment));
                $chatFile->save();
            }
        }
        // Pusher
        $pusher = new Pusher(
            config('messenger.pusher.app_key'),
            config('messenger.pusher.app_secret'),
            config('messenger.pusher.app_id'),
            [
                'cluster' => config('messenger.pusher.options.cluster')
            ]
        );
        $pusher->trigger('messenger-channel', 'messenger-event', [
            'message' => $message,
            'senderId' => $authId,
            'withId' => $withId
        ]);

        return response()->json([
            'success' => true,
            'message' => $message
        ], 200);
        return response()->json($user);


    }

    public function Deliver(Request $request)
    {
        $offer = Offer::where('id', $request->offer_id)->first();
        $offer->status = 3;
        $offer->save();
        return redirect()->back();
    }

    public function accepted(Request $request)
    {

        $offer = Offer::where('id', $request->offer_id)->first();
        $offer->count = $offer->count + 1;
        $offer->save();
        if ($offer->count == 2) {
            $offer->status = 5;
            $offer->count == 0;
        } else {
            $offer->status = 4;
        }
        $offer->save();
            $orderItems = OfferItem::where('service_id', $request->service_id)->where('offer_id', $request->offer_id)->first();
            $orderItems->count = $orderItems->count + 1;
            $orderItems->save();
            if ($orderItems->count == 2) {
                $orderItems->status = 5;
                $orderItems->count == 0;
            } else {
                $orderItems->status = 4;
        }
        $orderItems->save();
        $delivery = Delivery::where('id', $request->delivery_id)->first();
        $delivery->status = 1;
        $delivery->save();


//        return redirect(route('reviews'));
        return redirect()->back();
    }

    public function reject(Request $request)
    {
        $offer = Offer::where('id', $request->offer_id)->first();
        $offer->Delete();
        $orderItem = OfferItem::where('id', $request->offer_id)->get();
        foreach ($orderItem as $item){
            $item=OfferItem::where('id',$item->id)->first();
            $item->Delete();
         }
        return redirect()->back();
        return redirect(route('offersReceived'));
    }

    public function DeliveryReject(Request $request)
    {
//        return $request;
        $offer = Offer::where('id', $request->offer_id)->first();
         $offer->status = 2;
        $offer->save();
        $orderItems = OfferItem::where('service_id', $request->service_id)->where('offer_id',$request->offer_id)->first();
        $orderItems->status = 2;
        $orderItems->save();
        $delivery = Delivery::where('id', $request->delivery_id)->first();
        $delivery->status = 2;
        $delivery->save();
    }

    public function chatCreate(Request $request)
    {
//        return $request;
        $userAuth = Auth::user();

//        if($chat){
//            $chat->users()->sync($chat->id);
//        }else{
//            $chat = new Chat();
//            $chat->sender_id = $offer->sender_id;
//            $chat->reciever_id = $offer->receiver_id;
//            $chat->save();
//            $chat->users()->sync($chat->id);
//        }
        $userAuth->load('sendchats', 'recievedChats', 'sendchats.chatfiles');
//        return $userAuth;
        if ($request->file('file')) {
            foreach ($request->file as $file) {
                $chatFile = new ChatFile();
                $chatFile->chat_id = $chat->id;
                $file_extension = $file->getClientOriginalExtension();
                $file_path = $file->getFilename();
                $filename = $chat->id . '_' . Str::random(8) . '.' . $file_extension;
                $chatFile->file_name = $filename;
                Storage::disk('chatFile')->put($filename, File::get($file));
                $chatFile->save();
            }
        }
    }

    public function chat()
    {
        $user = Auth::user();
//        $user->load('sendchats','recievedChats','chatfiles');
        return $user;
        $offer = Offer::where('sender_id', $user->id)->orWhere('receiver_id', $user->id)->where('status', 2)->first();
//        $conversations = Chat::conversations()->setParticipant($user)->isPrivate()->get();
        return view('admin.chats.index', compact('offer'));
    }
}
