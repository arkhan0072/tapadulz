<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Package;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Auth;
use App\User;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Config;
use Setting;
use App\Events\OrderCreated;
class AddMoneyController extends HomeController
{
    protected $_api_context;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
       public function __construct()
    {
//        parent::__construct();

        /** setup PayPal api context **/

        $paypal_conf = [
            'client_id' => \Setting::get('PAYPAL_CLIENT_ID'),
            'secret' => \Setting::get('PAYPAL_SECRET'),
            'settings' => array(
                'mode' => \Setting::get('PAYPAL_IS_LIVE') ? 'live':'sandbox',
                'http.ConnectionTimeOut' => 30,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'ERROR'
            ),
        ];
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    /**
     * Show the application paywith paypalpage.
     *
     * @return \Illuminate\Http\Response
     */
    public function payWithPaypal()
    {
        return view('paywithpaypal');
    }

    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithpaypal(Request $request)
    {
//        return 'Ok';
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();

        $item_1->setName(\Auth::user()->name)/** item name **/
        ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice(10);


        /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
        ->setTotal(10);


        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your Going to be a premium member');


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('checkout.payment'))/** Specify return URL **/
        ->setCancelUrl(URL::route('uhome'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        }
        catch(\Exception $exception) {

            return $exception->getData();

            return $exception->getMessage();

        }
        catch
        (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('checkout.paywithpaypal');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('checkout.paywithpaypal');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('checkout.paywithpaypal');
    }

    public function getPaymentStatus(Request $request)
    {

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
//        Session::forget('paypal_payment_id');
        if (!$request->token) {
            \Session::put('error', 'Payment failed');
//            return Redirect::route('checkout.paywithpaypal');
        }
        $payment = Payment::get($request->paymentId, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);
        /**Execute the payment **/
        try{
            $result = $payment->execute($execution, $this->_api_context);
        }catch (\Exception $e){
            return redirect()->route('cart');
        }
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {

            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            $payment = new \App\Payment;
            $package=Package::where('id',1)->first();
            $payment->paypal_token = $request->token;
            $payment->verified = $request->PayerID;
            $payment->user_id =  \Auth::User()->id;
            $payment->amount =  $package->amount;
            $payment->save();

            if($payment){
                $user=\Auth::user();
                $user->is_premium=1;
                $user->subscribe_at=Carbon::now();
                $user->expires_at=Carbon::now()->addMonth();
                $user->save();
            }

            \Session::put('success', 'Payment success');
            return redirect(route('user.home'))->with(['Message'=>'Now Your Premium User.']);

//            return redirect()->route('orders.index');

//            return Redirect::route('checkout.paywithpaypal');
        }
        \Session::put('error', 'Payment failed');
//        return redirect()->route('cart');
        return Redirect::route('checkout.paywithpaypal');
    }
}
