<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Picture;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use BaklySystems\LaravelMessenger\Models\Message;
use BaklySystems\LaravelMessenger\Facades\Messenger;
use BaklySystems\LaravelMessenger\Models\Conversation;
use Image;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
//        //        if ($user->user_level == 1) {
        return view('admin.users.index');
//        } else {
//            return redirect()->back();
//        }
    }

    public function Userajax()
    {
        $users = User::where('id', '>', 0)->with('serceives', 'skills', 'picture', 'offer', 'city.country')->get();

//        return $users;
        return Datatables::of($users)
            ->addColumn('action', function (User $user) {
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="' . route("users.edit", $user->id) . '"><i class="fa fa-edit"></i></a>';
                return $btn . view('admin.users.delete', compact('user'))->render();
            })
            ->addColumn('description', function (User $user) {
                return $details = str_replace('&nbsp;', ' ', $user->description);
            })
            ->addColumn('status', function (User $user) {
                if ($user->status == 0) {
                    $status = '<svg height="100" width="100">
                              <circle cx="50" cy="50" r="20" stroke="black" stroke-width="1" fill="grey" />
                               </svg> ';
                } else {
                    $status = '<svg height="100" width="100">
                              <circle cx="50" cy="50" r="20" stroke="black" stroke-width="1" fill="#4ee44e" />
                               </svg> ';
                }
                return $status;
            })
            ->rawColumns(['status', 'details', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->user_level == 1) {
        $countries = Country::all();
        $cities = City::all();
        return view('admin.users.create', compact('countries', 'cities'));
        } else {
        return redirect()->back()->with(['msg', 'You Dont have Permission']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'user_level' => ['required'],
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->passsword);
        $user->user_level = $request->user_level;
        $user->save();
        return redirect(route('users.index'))->with('msg', 'User Created Successfuly');

    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function edit(User $user)
    {
//        return $user;
        $auth = Auth::user();
        $cities = City::all();
        $countries = Country::all();
        if ($auth->user_level == 1) {
        return view('admin.users.edit', compact('user', 'countries', 'cities'));
        }elseif($auth->user_level==0 && $user->id==Auth::user()->id) {
            $user=Auth::user();
            return view('admin.users.edit', compact('user', 'countries', 'cities'));
        }else{
            return redirect()->back()->with(['msg', 'You Dont have Permission']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'user_level' => ['required'],

        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->description = strip_tags($request->description);
        $user->user_level = $request->user_level;
        $user->save();
        if($request->has('picture')) {
            $picture = Picture::where('user_id', $user->id)->first();
            if ($picture) {
                $picture;
            } else {
                $picture = new Picture();
            }
            $image       = $request->file('picture');
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(67, 66);
            $file_extension = $image->getClientOriginalExtension();
            $filename = 'tapadul_' . Str::random(8) . '.' . $file_extension;
            $image_resize->save(public_path('images/users/' .$filename));
            $picture->user_id=$user->id;
            $picture->name=$filename;
           $picture->save();
        }
        if($user->user_level==1) {
            return redirect(route('users.index'))->with('msg', 'User Updated Successfully');
        }else{
        return redirect()->back();
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect(route('users.index'))->with('msg', 'User deleted Successfuly');
    }


    public function userprofile(Request $request)
    {
        $user = User::where('id',$request->id)->first();
        $user->load('city','serceives');
        $services=Service::where('id','>',0)->get();

        $services->load('user');
//        return gettype($services);
        return view('admin.users.profile', compact('user','services'));
    }

    public function changepassword()
    {
        $user = Auth::user();
        return view('changepass', compact('user'));
    }

    public function updatePassword(Request $request, User $user)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->back();
    }
    public function updateuserProfile(Request $request)
    {
        $user=Auth::user();
        $user->load('city','picture');
        return view('admin.users.editprofile', compact('user'));
    }
    public function messages(){
        $withUser = config('messenger.user.model', 'App\User')::findOrFail(Auth::id());
        $threads  = Messenger::threads(auth()->id());
        return view('frontend.messages',compact('withUser','threads'));
//        return $threads;
    }

}
