<?php

namespace App\Http\Controllers;

use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Picture;
use Illuminate\Support\Facades\DB;

class FrontEndController extends Controller
{
    public function index(){
        $services=Service::where('id','>',0)->with('user','pictures','ratings')->get();
$ratingz=[];
$rating1='';
        foreach ($services as $service){
        foreach ($service->ratings as $key=>$rating){
           $ratings= DB::table('ratings')
          ->select( DB::raw('COUNT(q1) as count_of_q1,SUM(q1) as q1_total,COUNT(q2) as count_of_q2,SUM(q2) as q2_total,COUNT(q3) as count_of_q3,SUM(q3) as q3_total'))
          ->where('service_id',  $rating->service_id)
          ->get();
            $qSum=$ratings[0]->count_of_q1+$ratings[0]->count_of_q2+$ratings[0]->count_of_q3;
            $qtotal=$ratings[0]->q1_total+$ratings[0]->q2_total+$ratings[0]->q3_total;
            $rating1=round($qtotal/$qSum);
        }
            array_push($ratingz,$rating1);
            }

        return view('index1',compact('services','ratingz'));
    }
    public function profile($username){
        $user=User::where('username',$username)->first();
        $user->load('picture');
        $services=Service::where('created_by',$user->id)->get();
        $services->load('user','pictures');
        return view('frontend.profile',compact('user','services'));
    }

    public function details($slug){
        $service=Service::where('slug',$slug)->first();
        $service->load('user','pictures');
        return view('frontend.gig-opening',compact('service'));
    }
    public function viewgig($slug){
        $service=Service::where('slug',$slug)->first();
        $service->load('user','pictures');
        $services=Service::where('created_by',Auth::id())->get();
        $btn=view('admin.services.offer', compact('service','services'))->render();
        return view('frontend.gigdetails',compact('service','services','btn'));
    }

}
