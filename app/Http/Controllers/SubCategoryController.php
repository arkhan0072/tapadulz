<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.subcategories.index');
    }
    public function Subcategoryajax()
    {
        $subCategory = SubCategory::where('id', '>', 0)->with('category');

//        return $users;
        return Datatables::of($subCategory)
            ->addColumn('action', function (SubCategory $subCategory) {
                $btn = '<a href="' . route("subcategories.show", $subCategory->id) . '"><i class="fa fa-eye"></i></a>
	   	<a href="' . route("subcategories.edit", $subCategory->id) . '"><i class="fa fa-edit"></i></a>';
                return $btn . view('admin.subcategories.delete', compact('subCategory'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return  view('admin.subcategories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request->validate([
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'string', 'max:255'],
            'category' => ['required'],
        ]);
        $subCategory = new SubCategory();
        $subCategory->name = $request->name;
        $subCategory->category_id = $request->category;
        $subCategory->save();
        return redirect(route('subcategories.index'))->with('msg', 'subCategory Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        return $subCategory;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        $categories=Category::all();
        return  view('admin.subcategories.edit',compact('subCategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        $request->validate([
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'string', 'max:255'],
            'category' => ['required'],
        ]);
        $subCategory->name = $request->name;
        $subCategory->category_id = $request->category;
        $subCategory->save();
        return redirect(route('subcategories.index'))->with('msg', 'subCategory Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        $subCategory->delete();
        return redirect(route('subcategories.index'))->with('msg', 'subCategory Deleted Successfully');

    }
    public function getsub(Request $request){
        $subcategories=SubCategory::where('category_id',$request->id)->get();
         $html=view('admin.services.subcategories',compact('subcategories'))->render();
        return $html;
    }
}
