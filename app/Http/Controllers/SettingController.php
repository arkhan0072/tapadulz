<?php

namespace App\Http\Controllers;
use Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public  function  show(){
        if(\Auth::user()->user_level == 1) {
            return view('admin.users.settings');
        }else{
            return redirect('user/home');
        }
    }
    public function update(Request $request){

        \Setting::set('PAYPAL_CLIENT_ID',$request->PAYPAL_CLIENT_ID);
        \Setting::set('PAYPAL_SECRET',$request->PAYPAL_SECRET);
        \Setting::set('PAYPAL_IS_LIVE',$request->PAYPAL_IS_LIVE);
        \Setting::save();
        return view('admin.users.settings');

    }

}
