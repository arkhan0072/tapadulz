<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
   protected $table='subcategories';
    public function service(){
        return $this->belongsTo('App\Service','subcategory_id');
    }
    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

}
