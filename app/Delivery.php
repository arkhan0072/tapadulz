<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public function files(){
        return $this->hasMany(Attachment::class);
    }
}
