<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{


    public function serviceNeeded(){
        return $this->belongsTo(Service::class,'serviceofferd_id');
    }


    public function serviceProvided(){
        return $this->belongsTo(Service::class,'serviceexchange_id');
    }

    public function orderitems(){
        return $this->hasMany('App\OfferItem');
    }

    public function offer(){
        return $this->belongsTo(Offer::class);
    }
}
