<Style>
    a.text-white.cancel.btn.btn-danger{
        line-height: 40px;
    }
    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
    }
    .br-theme-fontawesome-stars .br-widget a.br-selected:after {
        color:#F8DE08!important;
    }

</Style>
<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <?php if($order->provided): ?>
<div class="row">
<div class="col-md-12 col-lg-12">
    <div class="order-item">
        <div class="order-desc">
            <div class="clearfix">
                <div class="title">
                    <h4><?php echo e($order->provided->service->name); ?></h4>
                </div>

                <div class="status">
                    <?php if($order->provided->status==2): ?>
                        <p>Under Process</p>
                    <?php elseif($order->provided->status==3): ?>
                        <p>Delivered</p>
                    <?php elseif($order->provided->status==4): ?>
                        <p>Accepted</p>
                    <?php elseif($order->provided->status==5): ?>
                        <p>Completed</p>
                    <?php endif; ?>
                </div>
            </div>

            <div class="border"></div>
            <p><?php echo html_entity_decode(str_limit($order->provided->service->details,50,'...')); ?></p>
            <div class="clearfix">
                <div class="action clearfix">
                    <?php if($order->provided->status!=4): ?>
                    <div class="accept ml-4">
                        <a data-toggle="modal" data-target="#myModal<?php echo e($order->provided->id); ?>"  data-provided_id="<?php echo e($order->provided->id); ?>" class="deliver text-white">Deliver</a>
                            <div class="modal fade" id="myModal<?php echo e($order->provided->id); ?>" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Modal Header</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?php echo e(route('orderitemfile.store')); ?>" enctype="multipart/form-data" method="post">
                                            <?php echo csrf_field(); ?>
                                            <input type="hidden" name="provided_id" value="" class="provided_id">
                                            <textarea class="form-control" name="message" id="message" placeholder="Enter your text Message"></textarea>
                                            <input type="file" name="delivery[]" multiple>
                                            <input type="submit" value="Deliver">
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php endif; ?>

                    <div class="accept ml-4">
                        <a class="text-white" href="<?php echo e(route('history',$order->id)); ?>">Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>




























































</div>


























































</div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <script>
                        $(document).ready(function () {
                            $('.deliver').click(function (e) {
                                var provided_id=$(this).data('provided_id');
                                $('.provided_id').val(provided_id);
                            })
                        });
                        $(document).ready(function () {
                            $('.Accpet').click(function (e) {
                                e.preventDefault();
                                var provided_id=$(this).data('provided_id');
                                $.ajax({
                                    url:"<?php echo e(route('accepted')); ?>",
                                    type:'get',
                                    data:{
                                        provided_id:provided_id,
                                    },
                                    success:function (data) {
                                        console.log(data);
                                    }
                                })
                            });
                            $('.Reject').click(function (e) {
                                e.preventDefault();
                                var delivery_id=$(this).data('delivery_id');
                                var provided_id=$(this).data('provided_id');
                                var service_id=$(this).data('service_id');
                                $.ajax({
                                    url:"<?php echo e(route('DeliveryReject')); ?>",
                                    type:'get',
                                    data:{
                                        provided_id:provided_id,
                                    },
                                    success:function (data) {
                                        console.log(data);
                                    }
                                })
                            });
                            });


</script>
<?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/offers/offers_partial.blade.php ENDPATH**/ ?>