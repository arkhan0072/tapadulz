<?php if($messages): ?>
    <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($authId->id==$message->sender_id): ?>
                <div class="outgoing_msg">
                    <figure class="outgoing_msg_img">
                    <img src="<?php echo e(asset('public/images/users/')); ?>/<?php echo e($authId->picture ? $authId->picture->name:'default.png'); ?>" alt="" width="67" height="66">
            </figure>

                    <div class="sent_msg">
                        <div class="send_withd_msg">
                            <p title="<?php echo e(date('d-m-Y h:i A' ,strtotime($message->created_at))); ?>">
                                <?php echo html_entity_decode($message->message); ?>

                            </p>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                    <div class="incoming_msg">
                        <figure class="incoming_msg_img">
                            <img src="<?php echo e(asset('public/images/users/')); ?>/<?php echo e($withUser->picture ? $withUser->picture->name:'default.png'); ?>" alt="" width="67" height="66">
                        </figure>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                <p title="<?php echo e(date('d-m-Y h:i A' ,strtotime($message->created_at))); ?>">
                    <?php echo html_entity_decode($message->message); ?>

                </p>
                                </div>
                            </div>
                        </div>
            <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php endif; ?>

<?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/vendor/messenger/partials/messages.blade.php ENDPATH**/ ?>