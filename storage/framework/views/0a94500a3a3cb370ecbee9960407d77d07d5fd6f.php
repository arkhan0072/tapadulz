<a href="#" onclick="frmdlt<?php echo e($service->id); ?>.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt<?php echo e($service->id); ?>" action="<?php echo e(route('services.destroy', $service->id)); ?>" method="post">
    <?php echo method_field('delete'); ?>

    <?php echo e(csrf_field()); ?>

</form>
<?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/services/delete.blade.php ENDPATH**/ ?>