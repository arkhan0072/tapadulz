<?php $__env->startSection('css-styles'); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/vendor/messenger/css/messenger.css')); ?>">

<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <div class="menu"><!-- start .header-area -->
        <div class="container">
            <div class="menu-area">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="text-center text-white" href="#">Inbox</a>
                    </li>

                    <li class="nav-item marketing">
                        <a class="text-center text-white" href="#">Saved</a>
                    </li>

                    <li class="nav-item writing">
                        <a class="text-center text-white" href="#">Sent</a>
                    </li>

                    <li class="nav-item video">
                        <a class="text-center text-white" href="#">Peding</a>
                    </li>
                </ul>
            </div>
        </div>
    </div><!-- //end .header-area -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
<div class="col-md-4 threads">
    <div class="massage-area-sidebar">
        <div class="icons mx-auto">
            <ul class="nav">
                <li class="mx-3">
                    <a href="#">
                        <figure>
                            <img src="http://tapadulz.ar/public/images/plus.png" alt="" width="28"
                                 height="28">
                        </figure>
                    </a>
                </li>

                <li class="mx-3 mt-1">
                    <a href="#">
                        <figure>
                            <img src="http://tapadulz.ar/public/images/envelope.png" alt="" width="36"
                                 height="23">
                        </figure>
                    </a>
                </li>

                <li class="mx-3">
                    <a href="#">
                        <figure>
                            <img src="http://tapadulz.ar/public/images/user-icon.png" alt="" width="29"
                                 height="33">
                        </figure>
                    </a>
                </li>
            </ul>
        </div>

        <div class="box-wrap pr-3" id="boxscroll" tabindex="1" style="overflow: hidden; outline: none;">
            <?php echo $__env->make('messenger::partials.threads', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>

</div>
            <div class="col-md-8">
            <img src="<?php echo e(asset('public/images/chat.png')); ?>">
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/frontend/messages.blade.php ENDPATH**/ ?>