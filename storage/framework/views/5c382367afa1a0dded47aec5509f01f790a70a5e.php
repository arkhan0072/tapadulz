
<?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="row">
        <div class="col-md-12 col-lg-8">
            <div class="order-item">
                <div class="order-desc">
                    <div class="clearfix">
                        <div class="title">
                            <h4><a href="<?php echo e(route('viewgig',$offer->needed->service->slug)); ?>"><?php echo e($offer->needed->service->name); ?></a></h4>
                        </div>
                        <div class="status">
                            <?php if($offer->needed->status==1): ?>
                                <p>Pending</p>
                            <?php elseif($offer->status==2): ?>
                                <p>Under Process</p>
                            <?php elseif($offer->status==3): ?>
                                <p>Delivered</p>
                            <?php elseif($offer->status==4): ?>
                                <p>Accepted</p>
                            <?php elseif($offer->status==5): ?>
                                <p>Completed</p>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="border"></div>
                    <p><?php echo html_entity_decode(str_limit($offer->needed->service->details,50,'...')); ?></p>
                    <div class="clearfix">
                        <div class="action clearfix">
                            <?php if($offer->status==1): ?>
                                <div class="accept ml-4">
                                    <a href="#" class="text-white approved" title="Accept" data-sender_id="<?php echo e($offer->sender_id); ?>" data-receiver_id="<?php echo e($offer->receiver_id); ?>" data-offer_needed="<?php echo e($offer->needed->id); ?>" data-offer_provided="<?php echo e($offer->provided->id); ?>" data-id="<?php echo e($offer->needed->offer_id); ?>" >Accept</a>
                                </div>
                            <?php else: ?>
                                <div class="accept ml-4">
                                    <a href="<?php echo e(route("Deliver")); ?>" data-sender_id="<?php echo e($offer->sender_id); ?>" data-receiver_id="<?php echo e($offer->receiver_id); ?>" data-id="<?php echo e($offer->id); ?>" class="deliver text-white">Deliver</a>
                                </div>
                            <?php endif; ?>
                            <?php if($offer->status==6): ?>
                                <div class="reject ml-4">
                                    <a href="#" class="text-white cancel btn btn-danger" title="Cancel" data-sender_id="<?php echo e($offer->sender_id); ?>" data-receiver_id="<?php echo e($offer->receiver_id); ?>" data-id="<?php echo e($offer->id); ?>" >Cancel</a>
                                </div>
                            <?php else: ?>
                                <div class="reject ml-4">
                                    <a href="#" data-sender_id="<?php echo e($offer->sender_id); ?>" data-receiver_id="<?php echo e($offer->receiver_id); ?>" data-id="<?php echo e($offer->id); ?>" class="rejects text-white">Reject</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<script>
    $('.approved').click(function (e) {
        e.preventDefault();
        var offer_id=$(this).data('id');
        var sender_id=$(this).data('sender_id');
        var receiver_id=$(this).data('receiver_id');
        var offer_needed=$(this).data('offer_needed');
        var offer_provided=$(this).data('offer_provided');

        $.ajax({
            url:'<?php echo e(route('Approved')); ?>',
            type:'post',
            data: {
                "_token": "<?php echo e(csrf_token()); ?>",
                offer_id :offer_id,
                sender_id :sender_id,
                receiver_id :receiver_id,
                offer_needed :offer_needed,
                offer_provided :offer_provided
            },
            success:function (data) {
                console.log(data.username);
                $.ajax({
                    url:"<?php echo e(route('orderajax')); ?>",
                    type:'get',
                    success:function (data) {
                        $('.show_data').html(data);
                        window.location.href='<?php echo e(route('offersReceived')); ?>';
                    },
                });
            }
        });
    });
    $('.rejects').click(function (e) {
        e.preventDefault();
        var offer_id=$(this).data('id');
        var sender_id=$(this).data('sender_id');
        var receiver_id=$(this).data('receiver_id');
        // alert(token);
        $.ajax({
            url:'<?php echo e(route('Reject')); ?>',
            type:'post',
            data: {
                "_token": "<?php echo e(csrf_token()); ?>",
                offer_id :offer_id,
                sender_id :sender_id,
                receiver_id :receiver_id
            },
            success:function (data) {
                console.log(data);
                $.ajax({
                    url:"<?php echo e(route('orderajax')); ?>",
                    type:'get',
                    success:function (data) {
                        $('.show_data').html(data);
                    },
                });
                // location.reload();
            }
        });
    });
</script>
<?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/offers/offers_results.blade.php ENDPATH**/ ?>