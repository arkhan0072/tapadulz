
<a href="#" data-toggle="modal" data-target="#exampleModal<?php echo e($service->id); ?>"><i class="fa fa-envelope"></i></a>
<div class="modal fade" id="exampleModal<?php echo e($service->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  method="Post">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <th>Service</th>
                        <th>Duration</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                    <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $servicz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                        <td><?php echo e($servicz->name); ?></td>
                        <td><?php echo e($servicz->duration); ?></td>
                            <td><input type="submit" class="btn btn-primary idpass" name="Exchange Offer" data-id="<?php echo e($service->id); ?>" data-reciever_id="<?php echo e($service->created_by); ?>" data-service_id="<?php echo e($servicz->id); ?>" value="Send offer">

                        </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </form>
            </div>
            <div class="modal-footer">


            </div>
        </div>
    </div>
</div>



<?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/services/offer.blade.php ENDPATH**/ ?>