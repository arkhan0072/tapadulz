<?php $__env->startSection('content'); ?>
    <section class="order"><!-- start .order -->
        <div class="container">
            <div class="order-area mx-auto">
































                <div class="show_data">

                </div>
            </div>
        </div>
    </section><!-- //end .order -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        $(document).ready(function () {
            $.ajax({
                url:"<?php echo e(route('ajaxOffers')); ?>",
                type:'get',
                success:function (data) {
                    $('.show_data').html(data);
                },
            });
            });
            $(document).click(function () {
            $('.approved').click(function (e) {
                e.preventDefault();
                var offer_id=$(this).data('id');
                var sender_id=$(this).data('sender_id');
                var receiver_id=$(this).data('receiver_id');

                $.ajax({
                    url:'<?php echo e(route('Approved')); ?>',
                    type:'post',
                    data: {
                        "_token": "<?php echo e(csrf_token()); ?>",
                        offer_id :offer_id,
                        sender_id :sender_id,
                        receiver_id :receiver_id
                    },
                    success:function (data) {
                        console.log(data.username);
                        $.ajax({
                            url:"<?php echo e(route('ajaxOffers')); ?>",
                            type:'get',
                            success:function (data) {
                                $('.show_data').html(data);
                            },
                        });
                    }
                });
            });
            $('.rejects').click(function (e) {
                e.preventDefault();
                var offer_id=$(this).data('id');
                var sender_id=$(this).data('sender_id');
                var receiver_id=$(this).data('receiver_id');
                // alert(token);
                $.ajax({
                    url:'<?php echo e(route('Reject')); ?>',
                    type:'post',
                    data: {
                        "_token": "<?php echo e(csrf_token()); ?>",
                        offer_id :offer_id,
                        sender_id :sender_id,
                        receiver_id :receiver_id
                    },
                    success:function (data) {
                        console.log(data);
                        $.ajax({
                            url:"<?php echo e(route('ajaxOffers')); ?>",
                            type:'get',
                            success:function (data) {
                                $('.show_data').html(data);
                            },
                        });
                        // location.reload();
                    }
                });
            });

       });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/offers/recieved.blade.php ENDPATH**/ ?>