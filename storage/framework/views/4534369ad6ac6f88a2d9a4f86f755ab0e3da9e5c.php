<?php $__env->startSection('content'); ?>
    <section class="description"><!-- start .description -->
        <div class="container">
            <div class="main mx-auto">
                <div class="description-area mx-auto"></div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="sidebar">
                            <?php if(\Illuminate\Support\Facades\Auth::user()): ?>
                                <div class="item">
                                    <div class="exchange-link mx-auto">
                                        <a class="text-center text-white" href="<?php echo e(route('services.create')); ?>">Add New
                                            Service</a>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="item bg-gray">
                                <h5 class="text-center">Do you have any other requirement?</h5>
                                <div class="quote-link mx-auto">
                                    <a class="text-white" href="#">Get a quote</a>
                                </div>
                            </div>

                            <div class="item">
                                <h4>About the seller</h4>

                                <figure class="mx-auto">
                                    <img src="<?php echo e(asset('images/user3.png')); ?>" alt="" width="193" height="190">
                                </figure>

                                <div class="user-name">
                                    <h5 class="text-center mb-0"><?php echo e($user->name); ?></h5>
                                    <p class="text-center"><?php echo e($user->username); ?></p>
                                </div>

                                <div class="rating clearfix">
                                    <div class="clearfix">
                                        <ul class="mx-auto">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>

                                    <p class="text-center">5.0 of 160 reviews</p>
                                </div>

                                <ul>
                                    <li class="clearfix">
                                        <div class="left">
                                            <span>From</span>
                                        </div>

                                        <div class="right">
                                            <span><?php echo e($user->city ? $user->city->country->name:''); ?></span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Member Since</span>
                                        </div>

                                        <div class="right">
                                            <span><?php echo e(\Carbon\Carbon::parse($user->created_at)->format('Y')); ?></span>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="left">
                                            <span>Avg. Response Time</span>
                                        </div>

                                        <div class="right">
                                            <span>1 Hrs</span>
                                        </div>
                                    </li>
                                </ul>


                                <p><?php echo e($user->description); ?></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                    <?php if(Session::has('message')): ?>
                        <div class="alert alert-<?php echo e(Session::get('message-type')); ?> alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="glyphicon glyphicon-<?php echo e(Session::get('message-type') == 'success' ? 'ok' : 'remove'); ?>"></i> <?php echo e(Session::get('message')); ?>

                        </div>
                    <?php endif; ?>
                    <table id="service-datatable" class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Category</th>

                            <th>Duration</th>
                            <th>Location</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <!-- third party js -->
    <script src="<?php echo e(URL::asset('public/ubold/assets/libs/datatables/datatables.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('public/ubold/assets/libs/pdfmake/pdfmake.min.js')); ?>"></script>
    <!-- third party js ends -->

    <!-- Datatables init -->
    <script src="<?php echo e(URL::asset('public/ubold/assets/js/pages/datatables.init.js')); ?>"></script>
    <script>
       $(document).ready(function () {


            $('#service-datatable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "<?php echo e(route('Serviceajax')); ?>",
                buttons: [
                    
                        
                        
                        
                        
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id',title:'id'},
                    {data: 'name', name: 'name',title:'Name'},
                    // {data: 'details', name: 'details',title:'Description'},
                    {data: 'Category', name: 'Category',title:'Category'},
                    // {data: 'subcategory.category.name', name: 'subcategory.category.name',title:'Category'},
                    // {data: 'subcategory.name', name: 'subcategory.name',title:'SubCategory'},
                    {data: 'duration', name: 'duration',title:'Duration'},
                    {data: 'Location', name: 'Location',title:'Location'},
                    // {data: 'city.name', name: 'city.name',title:'City'},
                    // {data: 'city.country.name', name: 'city.country.name',title:'Country'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    <script>
        $(document).click(function () {


            $('.idpass').click(function (e) {
                e.preventDefault();
                var id=$(this).data('id');
                var service_id=$(this).data('service_id');
                var token=$('#token').val();
                var reciever_id=$(this).data('reciever_id');
                // alert(token);
                $.ajax({
                    url:'<?php echo e(route('offers.store')); ?>',
                    type:'post',
                    data: {
                        _token: token,
                        id :id,
                        service_id :service_id,
                        reciever_id :reciever_id
                    },
                    success:function (data) {
                        console.log(data);
                        location.reload();
                    }
                });
            });
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/services/index.blade.php ENDPATH**/ ?>