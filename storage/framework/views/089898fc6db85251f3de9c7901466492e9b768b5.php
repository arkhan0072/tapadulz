<?php $__env->startSection('content'); ?>
    <section class="order"><!-- start .order -->
        <div class="container">
            <div class="order-area mx-auto">
                <div class="show_data">

                </div>
            </div>
        </div>
    </section><!-- //end .order -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        $(document).ready(function () {
            $.ajax({
                url:"<?php echo e(route('orderajax')); ?>",
                type:'get',
                success:function (data) {
                    $('.show_data').html(data);
                },
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/admin/offers/offers.blade.php ENDPATH**/ ?>