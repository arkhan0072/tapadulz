

    <?php $__currentLoopData = $threads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $thread): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php if($thread->lastMessage): ?>

            <a href="/messenger/t/<?php echo e($thread->withUser->username); ?>" class="thread-link">
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="clearfix mb-2">
                            <figure class="chat_img">
                            <img src="<?php echo e(asset('public/images/users/')); ?>/<?php echo e($thread->withUser->picture ? $thread->withUser->picture->name:'default.png'); ?>" alt="" width="67" height="66">
                            </figure>
                            <div class="chat_ib">
                                <h5><?php echo e($thread->withUser->username); ?> </h5>
                                <?php if($thread->withUser->status==1): ?>
                                    <span class="chat_date">Online</span>
                                <?php else: ?>
                                    <span class="chat_date">Offline</span>
                                <?php endif; ?>
                            </div>

                            <p>  <?php if($thread->lastMessage->sender_id === auth()->id()): ?>
                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                <?php endif; ?>
                                <?php echo html_entity_decode(substr($thread->lastMessage->message, 0, 20)); ?>

                                <?php if(!$thread->lastMessage->is_seen &&
                                  $thread->lastMessage->sender_id != auth()->id()): ?>
                                    (<i class="fa fa-check"></i>)
                                <?php else: ?>
                                    (<i class="fa fa-check"></i><i class="fa fa-check"></i>)
                                <?php endif; ?></p>
                        </div>
                    </div>
                </div>

            </a>

<?php endif; ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php /**PATH C:\xampp\htdocs\tapadulz\resources\views/vendor/messenger/partials/threads.blade.php ENDPATH**/ ?>