<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Messenger Default User Model
    |--------------------------------------------------------------------------
    |
    | This option defines the default User model.
    |
    */

    'user' => [
        'model' => 'App\User'
    ],

    /*
    |--------------------------------------------------------------------------
    | Messenger Pusher Keys
    |--------------------------------------------------------------------------
    |
    | This option defines pusher keys.
    |
    */

    'pusher' => [
        'app_id'     => '972753',
        'app_key'    => '305edaba5f7fd9970f60',
        'app_secret' => 'fb5ab87f9bf3d4c656d0',
        'options' => [
            'cluster'   => 'eu',
            'encrypted' => true
        ]
    ],
];
