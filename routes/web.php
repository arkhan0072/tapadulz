<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include 'messenger.php';
Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = 'public/downloads/'. $filename;
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
})->name('download');
Route::get('welcome/{locale}', function ($locale) {
    App::setLocale($locale);
    return view('welcome');
    //
});
Route::get('/test', function () {
    return view('test');
});

Route::get('/support', function () {
    return view('frontend.support');
});
Route::get('/editprofile', function () {
    return view('admin.users.editprofile');
});
Route::get('/', 'FrontEndController@index')->name('index1');
Route::get('/profile/{username}', 'FrontEndController@profile')->name('profile');

Route::get('/viewgig/{slug}', 'FrontEndController@viewgig')->name('viewgig');
Route::get('/opengig/{slug}','FrontEndController@details')->name('gigOpen');

Route::post('/ajaxSearch','SearchController@ajaxSearch')->name('searchAjax');
Route::get('/search','SearchController@index')->name('search');
Auth::routes();
Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
Route::get('/order/history/{id}','OrderController@index')->name('history');
Route::get('/files/{name}','ChatFileController@file')->name('file');

Route::resource('services','ServiceController');
Route::get('/service/ajax','ServiceController@Serviceajax')->name('Serviceajax');
Route::get('/editprofile', 'UserController@updateuserProfile')->name('editprofile');

Route::resource('offers','OfferController');

Route::resource('orderitemfile','OrderItemFileController');

Route::get('/offer/ajax','OfferController@Offerajax')->name('Offerajax');
Route::get('/order/ajax','OfferController@offerz')->name('orderajax');
Route::get('/offers','OfferController@offer')->name('offers');

Route::resource('users','UserController');
Route::get('/user/ajax','UserController@Userajax')->name('Userajax');

Route::resource('categories','CategoryController');
Route::resource('ChatFile','ChatFileController');
Route::get('/category/ajax','CategoryController@Categoryajax')->name('Categoryajax');

Route::resource('subcategories','SubCategoryController');
Route::get('/subcategory/ajax','SubCategoryController@Subcategoryajax')->name('Subcategoryajax');

Route::resource('skills','SkillController');
Route::get('/skill/ajax','SkillController@Skillajax')->name('Skillajax');

Route::get('/user/profile/{id}','UserController@userprofile')->name('userProfile');
    Route::get('/home', 'FrontEndController@index')->name('ahome');


    Route::get('/chat', 'OfferController@chat')->name('chat');

    Route::post('/chatCreate', 'OfferController@chatCreate')->name('chatCreate');


    Route::get('orders/received','OfferController@index')->name('offersReceived');
    Route::get('offers/ajax','OfferController@ajaxOffers')->name('ajaxOffers');
    Route::post('/Approved','OfferController@accpet')->name('Approved');
    Route::post('/Reject','OfferController@reject')->name('Reject');
    Route::post('/DeliveryReject','OfferController@DeliveryReject')->name('DeliveryReject');
    Route::post('/Deliver','OfferController@Deliver')->name('Deliver');
    Route::get('/accepted','OfferController@accepted')->name('accepted');
    Route::get('/reviews','OfferController@review')->name('Review');

    Route::get('paywithpaypal', array('as' => 'checkout.paywithpaypal','uses' => 'AddMoneyController@payWithPaypal',));
    Route::post('paypal', array('as' => 'checkout.payment','uses' => 'AddMoneyController@postPaymentWithpaypal',));
    Route::get('paypal', array('as' => 'checkout.status','uses' => 'AddMoneyController@getPaymentStatus',));

    Route::get('/settings', 'SettingController@show')->name('settings.show');
    Route::post('/settings', 'SettingController@update')->name('settings.update');
    Route::get('/getsubcat','SubCategoryController@getsub')->name('getsubcat');
    Route::get('/subcities','CityController@getCity')->name('getsubcities');
//    Route::get('offers/received','OfferController@index');


});
Route::group(['prefix' => 'user',  'middleware' => 'auth'], function()
{
    Route::get('/editprofile', 'UserController@updateuserProfile')->name('editprofile');
    Route::get('/messages', 'UserController@messages')->name('messages');
    Route::resource('services','ServiceController');
    Route::get('/service/ajax','ServiceController@Serviceajax')->name('Serviceajax');
    Route::get('/order/history/{id}','OrderController@index')->name('history');

    Route::resource('users','UserController');
    Route::get('/user/ajax','UserController@Userajax')->name('Userajax');

    Route::resource('categories','CategoryController');
    Route::get('/category/ajax','CategoryController@Categoryajax')->name('Categoryajax');

    Route::resource('subcategories','SubCategoryController');
    Route::get('/subcategory/ajax','SubCategoryController@Subcategoryajax')->name('Subcategoryajax');

    Route::resource('skills','SkillController');
    Route::get('/skill/ajax','SkillController@Skillajax')->name('Skillajax');

    Route::get('/profile/{id}','UserController@userprofile')->name('Profile');

    Route::get('/home', 'FrontEndController@index')->name('uhome');
    Route::get('/chat', 'OfferController@chat')->name('chat');
    Route::post('/chatCreate', 'OfferController@chatCreate')->name('chatCreate');

    Route::get('orders/received','OfferController@index');
    Route::get('offers/ajax','OfferController@ajaxOffers')->name('ajaxOffers');
    Route::get('/order/ajax','OfferController@offerz')->name('orderajax');
    Route::get('/offers','OfferController@offer')->name('offers');
    Route::post('/Approved','OfferController@accpet')->name('Approved');
    Route::post('/reject','OfferController@reject')->name('Reject');

    Route::get('paywithpaypal', array('as' => 'checkout.paywithpaypal','uses' => 'AddMoneyController@payWithPaypal',));
    Route::post('paypal', array('as' => 'checkout.payment','uses' => 'AddMoneyController@postPaymentWithpaypal',));
    Route::get('paypal', array('as' => 'checkout.status','uses' => 'AddMoneyController@getPaymentStatus',));

    Route::get('/settings', 'SettingController@show')->name('settings.show');
    Route::post('/settings', 'SettingController@update')->name('settings.update');
    Route::get('orders/received','OfferController@index')->name('offersReceived');
    Route::get('/getsubcat','SubCategoryController@getsub')->name('getsubcat');
    Route::get('/accepted','OfferController@accepted')->name('accepted');
    Route::post('/DeliveryReject','OfferController@DeliveryReject')->name('DeliveryReject');
    Route::resource('ratings','RatingController');


});
Route::get('/gprofile/{id}','HomeController@gprofile')->name('GetProfile');

